from datetime import timedelta
import pathlib

import constants

class QueryResult(object):
    def __init__(self):
        self.cache_used = False
        self.cache_activated = True
        self.cache_dir = pathlib.Path.home().joinpath(constants.PROGRAM_DIR_ROOT).joinpath(constants.PROGRAM_DIR_CACHE)
        self.cache_expiration = timedelta(days=7)
        self.result_transformed = None
        self.result_pure = None
        self.errors = []
        self.warnings = []
        self.transformed_ouput_format = 'XML'
        self.query_cache_file_path_error = None
        self.query_cache_file_path = None
        self.query_cache_file_mtime = None
        self.query_original = None
        self.query_transformed = None
        self.xpath = None
        self.xpath_params = {}
        self.xsl_file = None
        self.xsl_file_query = None
        self.xsl_file_xpath = None
        self.xsl_file_xpath_params = None
        self.qualys_parameters = {}
        self.qualys_call = None

    def add_error(self, error_id: str, error_message: str):
        self.errors.append((error_id, error_message))

    def without_errors(self):
        return len(self.errors) == 0

    def add_warning(self, warning_id: str, warning_message: str):
        self.warnings.append((warning_id, warning_message))

    def without_warnings(self):
        return len(self.warnings) == 0

    def cache_file(self):
        return self.cache_dir.joinpath(self.query_cache_file_path)