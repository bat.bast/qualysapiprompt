import qualysapi
import requests
import pathlib
from datetime import datetime
import lxml.etree as ET

from completion import QUALYS_ID_VERBS, QUALYS_VERBS, QUALYS_TOOLS, QUALYS_COMPARATORS, QUALYS_OBJECTS, \
    QUERY_DICT_QUALYS, QUALYS_COMPARATORS_DICT, QUALYS_ATTRIBUTES
import utils, constants
from query_result import QueryResult


class Client(object):
    def __init__(self):
        self.qualys_connection = qualysapi.connect(
            pathlib.Path.home().joinpath(constants.PROGRAM_DIR_ROOT).joinpath(constants.PROGRAM_QUALYS_CONFIG))

    def query(self, query_result: QueryResult, previous_results=None):
        # Buid Qualys Request from Query
        self._make_call_args(query_result, previous_results)

        # Fetch Qualys API Result
        if query_result.without_errors():
            query_result.query_cache_file_path = utils.query_to_cache_filename(query_result.query_transformed)
            # Call Qualys API
            self._call_qualys_api(query_result)

    def _get_qualys_api_data(self, query_result: QueryResult):
        now = datetime.now()
        cache_file_expired = False

        if query_result.cache_file().is_file():
            cache_file_mtime = datetime.fromtimestamp(query_result.cache_file().stat().st_mtime)
            cache_file_expired = (now - cache_file_mtime) > query_result.cache_expiration

        if query_result.cache_activated and query_result.cache_file().is_file() and not cache_file_expired:
            query_result.cache_used = True
            with open(query_result.cache_file(), 'r', encoding="utf-8") as f:
                query_result.result_pure = f.read()
        else:
            query_result.cache_used = False
            query_result.result_pure = self.qualys_connection.request(query_result.qualys_call,
                                                                      query_result.qualys_parameters)
            with open(query_result.cache_file(), 'w', encoding='utf-8') as cfile:
                cfile.write(query_result.result_pure)

        query_result.query_cache_file_mtime = datetime.fromtimestamp(query_result.cache_file().stat().st_mtime)

    def _call_qualys_api(self, query_result: QueryResult):
        try:
            # Get Qualys API data from cache or Qualys API server
            self._get_qualys_api_data(query_result)

            # Test if the Pure Result (without transformation) is OK
            check_res = utils.check_result(query_result.result_pure)
            if check_res == 'XML':
                if query_result.xsl_file is not None:
                    # Transform Pure Result with XSLT
                    utils.xslt_qualys_xml(query_result)
                else:
                    query_result.transformed_ouput_format = 'XML'
            elif check_res == 'PDF':
                query_result.transformed_ouput_format = 'PDF'
                query_result.result_transformed = 'Result file is a PDF, see ' + query_result.query_cache_file_path
                query_result.add_warning('Request', query_result.result_transformed)
            else:
                index = 0
                for line in query_result.result_pure.split('\n'):
                    if line.find('<responseCode>') >= 0:
                        query_result.add_error('API',
                                               'Request --> call=' + query_result.qualys_call + ', parameters=' + str(
                                                   query_result.qualys_parameters) + '\n' + ET.fromstring(line).text)
                        break
                    # Error must be into the first 10 lines
                    if index > 10:
                        query_result.add_error('API',
                                               'Request --> call=' + query_result.qualys_call + ', parameters=' + str(
                                                   query_result.qualys_parameters) + '\nUNKNOWN ERROR --> Manually analyse response!')
                        break
                    index += 1
                # Rename result file with is not correct
                utils.rename_file_with_error(query_result)
                query_result.transformed_ouput_format = 'XML'
        except requests.exceptions.RequestException as re:
            query_result.add_error('API', 'Request --> call=' + query_result.qualys_call + ', parameters=' + str(
                query_result.qualys_parameters) + '\n' + repr(re))

    def _make_call_args(self, query_result: QueryResult, previous_results: dict = None):
        parameters = None
        conditions = []
        words = query_result.query_original.split()
        nb_words = len(words)
        tool, verb, obj = None, None, None
        api_version = '3.0'
        query_transformed = ''

        # Minimal 3 words are required, except if tool is report : minimal 2 words
        if nb_words < 2:
            query_result.add_error('QUERY', 'Minimal query: 2 words are required')
            return

        tool = words[0].lower()
        if tool not in QUALYS_TOOLS:
            query_result.add_error('KEYWORD', '{} {} not in {}'.format('Tool', tool, QUALYS_TOOLS))
        else:
            query_transformed += tool
            api_version = constants.QUALYS_API_VERSION[tool]

        if nb_words > 1:
            verb = words[1].lower()
            if verb not in QUALYS_VERBS:
                query_result.add_error('KEYWORD', '{} {} not in {}'.format('Verb', verb, QUALYS_VERBS))
            else:
                query_transformed += ' ' + verb

        if nb_words > 2 and tool == 'report' and (
                words[2].lower() in constants.QUALYS_FILTERS_LIST or words[2].startswith('XPath(')):
            # Add "fake" obj word which will not be used
            # ==> Shift right
            words.insert(2, 'fake_object')
            nb_words = len(words)

        if nb_words > 2 and words[2] != 'fake_object':
            obj = words[2].lower()
            if obj not in QUALYS_OBJECTS:
                query_result.add_error('KEYWORD', '{} {} not in {}'.format('Object', obj, QUALYS_OBJECTS))
            else:
                query_transformed += ' ' + obj

        if nb_words > 3:
            filter_or_xpath = words[3]
            xpath_in_query = filter_or_xpath.startswith('XPath(')
            if filter_or_xpath.lower() not in constants.QUALYS_FILTERS_LIST and not xpath_in_query:
                query_result.add_error('KEYWORD',
                                       '{} {} not in {}'.format('Filter or XPath', filter,
                                                                constants.QUALYS_FILTERS_LIST + ['XPath']))
            else:
                if xpath_in_query:
                    query_result.xpath = filter_or_xpath
                    pure_xpath, query_result.xpath_params = utils.extract_xpath_data(query_result.xpath)
                    query_result.xsl_file_xpath, query_result.xsl_file_xpath_params = utils.xpath_to_xsl_filename(
                        pure_xpath, query_result.xpath_params)
                    query_transformed += ' ' + query_result.xpath
                    xpath_count = 1
                else:
                    xpath_count = 0
                nb_conditions = int((len(words) - 3 - xpath_count) / 4)  # Take filter for the first group

                for condition in range(0, nb_conditions, 1):
                    conjuction = words[3 + xpath_count + condition * 4].lower()
                    if condition != 0:
                        if conjuction not in constants.CONJUCTIONS_LIST:
                            query_result.add_error('',
                                                   '{} {} not in {}'.format('Conjuction', conjuction,
                                                                            constants.CONJUCTIONS_LIST))
                        else:
                            query_transformed += ' ' + conjuction
                    else:
                        if conjuction not in constants.QUALYS_FILTERS_LIST:
                            query_result.add_error('KEYWORD',
                                                   '{} {} not in {}'.format('Filter', conjuction,
                                                                            constants.QUALYS_FILTERS_LIST))
                        else:
                            query_transformed += ' ' + conjuction

                    attribute = words[4 + xpath_count + condition * 4]
                    if attribute not in QUALYS_ATTRIBUTES:
                        query_result.add_error('KEYWORD',
                                               '{} {} not in {}'.format('Attribute', attribute, QUALYS_ATTRIBUTES))

                    comparator = words[5 + xpath_count + condition * 4].upper()
                    if comparator is not None and comparator not in QUALYS_COMPARATORS and comparator not in QUALYS_COMPARATORS.values():
                        query_result.add_error('KEYWORD',
                                               '{} {} not in {}'.format('Comparator', comparator, QUALYS_COMPARATORS))

                    term_right = words[6 + xpath_count + condition * 4]
                    # XPath extraction
                    if term_right.startswith('XPath(') and term_right.endswith(')'):
                        index = term_right.split('(')[1].split('/')[0]
                        try:
                            index_int = int(index)
                            xpath_r = '/' + '/'.join(term_right.split('(')[1].split(')')[0].split('/')[1:])
                            # If not, set to [0]
                            if index_int in previous_results and previous_results[index_int] is not None:
                                dom = ET.fromstring(previous_results[index_int].encode(encoding='utf-8'))
                                value_list = dom.xpath(xpath_r)
                                if value_list is not None and len(value_list) == 1:
                                    value = value_list[0].text
                                    conditions.append([attribute, comparator, value])
                                    term_right = value
                                elif value_list is not None and len(value_list) > 1:
                                    query_result.add_error('PREVIOUS_RESULT',
                                                           '{} nodes founded for XPath {} at index {} of previous results'.format(
                                                               str(len(value_list)), xpath_r, index))
                                else:
                                    query_result.add_error('PREVIOUS_RESULT',
                                                           'XPath {} not found at index {} of previous results'.format(
                                                               xpath_r, index))
                                # Clear variable
                                del dom
                            else:
                                query_result.add_error('PREVIOUS_RESULT',
                                                       'Index {} not found into previous results'.format(index))
                        except ValueError:
                            query_result.add_error('PREVIOUS_RESULT', 'Index {} must be an integer'.format(index))
                        except ET.XMLSyntaxError:
                            query_result.add_error('PREVIOUS_RESULT',
                                                   'Previous result at index {} must be a valid XML string'.format(
                                                       index))
                    conditions.append([attribute, comparator, term_right])
                    query_transformed += ' ' + attribute + ' ' + comparator + ' ' + term_right

        # Verify query from dict
        current_comparator_query_dict = None
        try:
            current_comparator_query_dict = QUALYS_COMPARATORS_DICT[tool][obj]
        except KeyError:
            # dict not found, try with only tool
            try:
                current_comparator_query_dict = QUALYS_COMPARATORS_DICT[tool]
            except KeyError:
                # dict not found, but sometimes, there is no comparator
                # query_result.add_error('QUERY', 'Dictionary not found for comparator sub-query with tool={} and object={}'.format(tool, obj))
                pass
        query_verification = utils.query_verify_dict(query_transformed, QUERY_DICT_QUALYS,
                                                     current_comparator_query_dict)
        if not query_verification:
            query_result.add_error('QUERY', 'Malformed query by comparison with QUERY_DICT_QUALYS')

        obj_txt = ('/' + obj) if obj is not None else ''
        query_result.xsl_file_query = '/' + tool + '/' + verb + obj_txt
        parameters = utils.build_parameters(conditions, api_version)
        if api_version == '3.0':
            query_result.qualys_call = '/' + verb + '/' + tool + obj_txt
            if verb in QUALYS_ID_VERBS:
                # Required id condition
                id_condition = None
                for condition in conditions:
                    if condition[0] == 'id' and (str(condition[1]) == constants.QUALYS_COMPARATORS['EQUALS'] or str(
                            condition[1]) == 'EQUALS'):
                        id_condition = condition[2]
                        break
                if id_condition is not None:
                    query_result.qualys_call += '/' + id_condition
                else:
                    query_result.add_error('KEYWORD', 'For request with this verb ' + verb + ', id must be present')
        elif api_version == '2.0':
            query_result.qualys_call = '/api/2.0/fo/' + tool + obj_txt
            parameters['action'] = verb

        query_result.xsl_file = query_result.xsl_file_query
        if query_result.xsl_file_xpath is not None:
            query_result.xsl_file += '_' + query_result.xsl_file_xpath
            if query_result.xsl_file_xpath_params is not None:
                query_result.xsl_file += '.' + query_result.xsl_file_xpath_params

        query_result.query_transformed = query_transformed
        query_result.qualys_parameters = parameters
