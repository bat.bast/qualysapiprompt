import lxml.etree as ET
from pathlib import Path
import pandas
from io import StringIO
import constants
import re
from typing import Optional, AnyStr

from query_result import QueryResult
from constants import QUALYS_COMPARATORS, CONJUCTIONS_LIST

_xpath_pattern = r'XPath\(([a-zA-Z\/0-9\-_]+)(.*)\)'


def count_words(sentence: str) -> int:
    return len(sentence.split())


def xpath_to_xsl_filename(pure_xpath: str, params: list):
    res_pure_xpath = None
    res_params = None
    if pure_xpath is not None:
        res_pure_xpath = 'XPath' + pure_xpath.replace('/', '.')
    if params is not None and len(params) > 0:
        res_params = 'params.' + '.'.join(params)
    return res_pure_xpath, res_params


def extract_xpath_data(xpath: str):
    if xpath is not None:
        m = re.match(_xpath_pattern, xpath, flags=re.IGNORECASE)
        if m is not None:
            pure_xpath = m.groups()[0]
            params = m.groups()[1].strip().split(',') if m.groups()[1] != '' else None
            dict_params = {}
            if params is not None:
                for param in params:
                    kv = param.split('=')
                    if len(kv) == 2:
                        dict_params[str(kv[0].strip())] = "'" + str(kv[1].strip()) + "'"
            return pure_xpath, dict_params
    return None, None


def query_to_cache_filename(query: str):
    if query is not None:
        # Remove XPath(...) --> Only used to choose XSL file for transformation
        return '.'.join(re.sub(_xpath_pattern, '', query.replace('/', '').replace(':', '_'),
                               flags=re.IGNORECASE).split()) + '.res'


def build_parameters(conditions: [], api_version: str = '3.0'):
    if api_version == '3.0':
        parameters = None
        if len(conditions) > 0:
            filters_text = ''
            for condition in conditions:
                operator = condition[1]
                if operator in constants.QUALYS_COMPARATORS.values():
                    for key, value in constants.QUALYS_COMPARATORS.items():
                        if operator == value:
                            operator = key
                            break
                operator = operator.replace('_', ' ')
                filters_text += '<Criteria field="' + condition[0] + '" operator="' + operator + '">' + condition[
                    2] + '</Criteria>'
            parameters = '<ServiceRequest><filters>' + filters_text + '</filters></ServiceRequest>'
        return parameters
    elif api_version == '2.0':
        parameters = {}
        if len(conditions) > 0:
            for condition in conditions:
                parameters[condition[0]] = condition[2]
        return parameters


def rename_file_with_error(query_result: QueryResult):
    query_result.query_cache_file_path_error = query_result.query_cache_file_path + '.error'
    query_result.add_error('API',
                           'Result data has error: rename ' + query_result.query_cache_file_path + ' to ' + query_result.query_cache_file_path_error)
    query_result.cache_file().replace(Path(query_result.cache_dir).joinpath(query_result.query_cache_file_path_error))


def xslt_qualys_xml(query_result: QueryResult):
    output_format_file = '_' + query_result.transformed_ouput_format.lower()
    if query_result.result_pure is not None:
        xsl_directory = str(Path(__file__).parent) + '/xsl'
        xsl_file = xsl_directory + query_result.xsl_file + output_format_file + '.xsl'
        xsl_file_query = xsl_directory + query_result.xsl_file_query + output_format_file + '.xsl'
        if query_result.xsl_file_xpath is not None:
            xsl_file_xpath = xsl_directory + query_result.xsl_file_query + '_' + query_result.xsl_file_xpath + output_format_file + '.xsl'
        else:
            xsl_file_xpath = xsl_file
        xsl_default_format_file = xsl_directory + '/' + constants.XSL_DEFAULT_FILE + output_format_file + '.xsl'
        xsl_default_xml_file = xsl_directory + '/' + constants.XSL_DEFAULT_FILE + '_xml.xsl'
        if Path(xsl_file).exists():
            new_xsl_path = str(Path(xsl_file).absolute())
        elif Path(xsl_file_xpath).exists():
            new_xsl_path = str(Path(xsl_file_xpath).absolute())
            query_result.add_warning('XSLT',
                                     'No XSL file for query "' + query_result.query_transformed +
                                     '" and output format ' +
                                     query_result.transformed_ouput_format +
                                     ' - Expected ' + xsl_file +
                                     ' --> Switch to ' + xsl_file_xpath)
        elif Path(xsl_file_query).exists():
            new_xsl_path = str(Path(xsl_file_query).absolute())
            query_result.add_warning('XSLT',
                                     'No XSL file for query "' + query_result.query_transformed +
                                     '" and output format ' +
                                     query_result.transformed_ouput_format +
                                     ' - Expected ' + xsl_file +
                                     ' --> Switch to ' + xsl_file_query)
        elif Path(xsl_default_format_file).exists():
            new_xsl_path = str(Path(xsl_default_format_file).absolute())
            query_result.add_warning('XSLT',
                                     'No XSL file for query "' + query_result.query_transformed +
                                     '" and output format ' +
                                     query_result.transformed_ouput_format +
                                     ' - Expected ' + xsl_file +
                                     ' --> Switch to ' + xsl_default_format_file)
        elif Path(xsl_default_xml_file).exists():
            new_xsl_path = str(Path(xsl_default_xml_file).absolute())
            query_result.add_warning('XSLT',
                                     'No XSL file for query "' + query_result.query_transformed +
                                     '" and output format ' +
                                     query_result.transformed_ouput_format +
                                     ' - Expected ' + xsl_file +
                                     ' --> Switch to XML format with ' + xsl_default_xml_file)
            query_result.transformed_ouput_format = 'XML'
        else:
            query_result.add_error('XSLT',
                                   'No XSL file for query "' + query_result.query_transformed +
                                   '" and output format ' +
                                   query_result.transformed_ouput_format +
                                   ' - Expected ' + xsl_file)
            return
        query_result.xsl_file = new_xsl_path
        try:
            xslt = ET.parse(query_result.xsl_file)
            dom = ET.fromstring(query_result.result_pure.encode(encoding='utf-8'))
            transform = ET.XSLT(xslt)
            # query_result.query_xsl_file_params must be a dict
            result_transform = transform(dom, **query_result.xpath_params)

            if query_result.transformed_ouput_format == 'XML':
                query_result.result_transformed = ET.tostring(result_transform, pretty_print=True).decode()
            else:
                query_result.result_transformed = str(result_transform)
        except ET.LxmlError as e:
            query_result.add_error('XSLT', 'Error into XSL ' + query_result.xsl_file + ' --> ' + repr(e))
        except ET.XMLSyntaxError as e:
            query_result.add_error('XSLT', 'Error into XSL ' + query_result.xsl_file + ' --> ' + repr(e))
        except TypeError as e:
            query_result.add_error('XSLT',
                                   'Error into XSL call, params ' + query_result.xpath_params + ' is not a dict' + ' --> ' + repr(
                                       e))
    elif query_result.result_pure is None:
        query_result.add_error('XSLT', 'No Result XML data')


def str_match( regex: str, data: str) -> Optional[AnyStr]:
    if regex is not None:
        return re.match(regex, str(data),re.MULTILINE|re.DOTALL)
    else:
        return data


def result_csv_to_tabular(result_csv, header_color: str, data_color: str, error_color: str,
                          debug_activated: bool = False):
    join_str = ' | '
    tabular = []
    result_strio = StringIO(result_csv)
    try:
        df = pandas.read_csv(result_strio, sep=",")
        columns = df.columns
        df = df.dropna(how='all').fillna('')
        tabular.append((header_color, join_str.join(columns) + '\n'))
        fragment_node_name = None
        for row in df.iterrows():
            # Search XML fragment
            if fragment_node_name is None:
                # Default: CSV row
                row_str = join_str.join(map(str, row[1].values))
                node_start_index = row_str.find('<')
                node_end_index = row_str.find('>')
                if node_end_index > node_start_index and node_start_index >= 0:
                    # Start of XML fragment
                    fragment_node_name = row_str[node_start_index + 1:node_end_index].split(' ')[0]
                    row_str = ''.join(map(str, row[1].values))
            else:
                row_str = ''.join(map(str, row[1].values))
                if row_str.find('</' + fragment_node_name + '>') >= 0:
                    # End of XML fragment
                    fragment_node_name = None
            tabular.append((data_color, row_str + '\n'))
    except pandas.errors.ParserError:
        if debug_activated:
            tabular.append((error_color, 'Error parsing CSV result into pandas.DataFrame\n' + result_csv))
        else:
            tabular.append(
                (error_color, 'Error parsing CSV result into pandas.DataFrame ; activate debug to see raw CSV data'))

    return tabular


def check_result(xml: str):
    for xml_segment in constants.EXPECTED_XML_RESULTS:
        if xml.find(xml_segment) >= 0:
            return 'XML'
    if xml.startswith('%PDF-'):
        return 'PDF'


def flatten_lists_qualys_query_terms(terms_list: list) -> list:
    # Flatten list of list
    if isinstance(terms_list, list):
        final_key_list = []
        for sublist in terms_list:
            if isinstance(sublist, list):
                for key in sublist:
                    if key is not None and '...' not in str(key) and not str(key).lower().startswith('my') and not str(
                            key).lower().startswith('xpath(') and str(
                        key).lower() not in constants.QUALYS_FILTERS_LIST and str(
                        key).lower() not in constants.CONJUCTIONS_LIST and str(
                        key).upper() not in constants.QUALYS_COMPARATORS and str(
                        key).upper() not in constants.QUALYS_COMPARATORS.values():
                        final_key_list.append(key)
            else:
                final_key_list.append(sublist)
        return final_key_list


def extract_dict_qualys_attributes(source_dict: dict) -> list:
    # query format
    # 0     1   2       3           3|4     4|5         5|6     6|7
    # tool verb object [XPath(...)] where attribute comparator value
    attributes_list = []
    for level in [4, 5]:
        attributes_list.append(extract_dict_level(source_dict, level))
    return flatten_lists_qualys_query_terms(attributes_list)


def extract_dict_level(source_dict: dict, level: int = 0) -> list:
    if source_dict is not None and isinstance(source_dict, dict):
        keys_list = []
        for key, value in source_dict.items():
            if level == 0:
                keys_list.append(key)
            else:
                keys_list.append(extract_dict_level(value, level - 1))
        return flatten_lists_qualys_query_terms(keys_list)


def query_verify_dict(query: str, dict_to_verify: dict, another_dict_comparator: dict = None) -> bool:
    if query is not None and isinstance(query, str) and dict_to_verify is not None and isinstance(dict_to_verify, dict):
        recurse_dict = dict_to_verify
        query_split = query.split()
        index = 0
        qualys_comparator_enabled = False
        for word in query_split:
            index += 1
            if word in constants.CONJUCTIONS_LIST:
                if another_dict_comparator is not None and isinstance(another_dict_comparator, dict):
                    recurse_dict = another_dict_comparator
                else:
                    return False
            else:
                if word.startswith('XPath('):
                    word = 'XPath(...)'
                try:
                    dict_value = recurse_dict[word]
                    if isinstance(dict_value, dict):
                        recurse_dict = dict_value
                    elif (dict_value is None or isinstance(dict_value, str)) and index == len(query_split):
                        return True
                    if word is not None and (
                            word in constants.QUALYS_COMPARATORS or word in constants.QUALYS_COMPARATORS.values()):
                        qualys_comparator_enabled = True
                    else:
                        qualys_comparator_enabled = False
                except KeyError:
                    # Terminal comparator element, choose by user
                    # dict must contains '...'
                    if index == len(query_split):  # and '...' in recurse_dict:
                        return True
                    elif qualys_comparator_enabled:  # and '...' in recurse_dict:
                        pass
                    else:
                        return False
        return True
    else:
        return False
