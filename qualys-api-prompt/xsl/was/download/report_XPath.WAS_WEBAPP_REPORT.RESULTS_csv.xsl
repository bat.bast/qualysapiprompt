<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="3.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output omit-xml-declaration="yes" method="text" indent="no"/>
    <xsl:key name="glossary_qid" match="/WAS_WEBAPP_REPORT/GLOSSARY/QID_LIST/QID" use="QID"/>
    <xsl:template match="/WAS_WEBAPP_REPORT">webapp_name,id,severity,status,category,title,qid,first_time_detected,last_time_detected,url<xsl:text>
</xsl:text>
        <xsl:for-each select="RESULTS/WEB_APPLICATION/VULNERABILITY_LIST/VULNERABILITY">
            <xsl:sort select="STATUS" data-type ="text" order="ascending" />
            <xsl:sort select="key('glossary_qid',QID)/SEVERITY" data-type ="number" order="descending" />
            <xsl:sort select="key('glossary_qid',QID)/CATEGORY" data-type ="text" order="ascending" />
            <xsl:apply-templates select="."/>
        </xsl:for-each>
        <xsl:for-each select="RESULTS/WEB_APPLICATION/INFORMATION_GATHERED_LIST/INFORMATION_GATHERED">
            <xsl:sort select="STATUS" data-type ="text" order="ascending" />
            <xsl:sort select="key('glossary_qid',QID)/SEVERITY" data-type ="number" order="descending" />
            <xsl:sort select="key('glossary_qid',QID)/CATEGORY" data-type ="text" order="ascending" />
            <xsl:apply-templates select="."/>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="VULNERABILITY">
        <xsl:value-of select="../../NAME"/><xsl:text>,</xsl:text>
        <xsl:value-of select="ID"/><xsl:text>,</xsl:text>
        <xsl:value-of select="key('glossary_qid',QID)/SEVERITY"/><xsl:text>,</xsl:text>
        <xsl:value-of select="STATUS"/><xsl:text>,</xsl:text>
        <xsl:value-of select="key('glossary_qid',QID)/CATEGORY"/><xsl:text>,</xsl:text>
        <xsl:value-of select="key('glossary_qid',QID)/TITLE"/><xsl:text>,</xsl:text>
        <xsl:value-of select="QID"/><xsl:text>,</xsl:text>
        <xsl:value-of select="FIRST_TIME_DETECTED"/><xsl:text>,</xsl:text>
        <xsl:value-of select="LAST_TIME_DETECTED"/><xsl:text>,</xsl:text>
        <xsl:value-of select="URL"/><xsl:text>
</xsl:text>
    </xsl:template>

    <xsl:template match="INFORMATION_GATHERED">
        <xsl:value-of select="../../NAME"/><xsl:text>,</xsl:text>
        <xsl:value-of select="ID"/><xsl:text>,</xsl:text>
        <xsl:value-of select="key('glossary_qid',QID)/SEVERITY"/><xsl:text>,</xsl:text>
        <xsl:text>,</xsl:text>
        <xsl:value-of select="key('glossary_qid',QID)/CATEGORY"/><xsl:text>,</xsl:text>
        <xsl:value-of select="key('glossary_qid',QID)/TITLE"/><xsl:text>,</xsl:text>
        <xsl:value-of select="QID"/><xsl:text>,</xsl:text>
        <xsl:text>,</xsl:text>
        <xsl:value-of select="LAST_TIME_DETECTED"/><xsl:text>,</xsl:text>
        <xsl:text>
</xsl:text>
    </xsl:template>
</xsl:stylesheet>