<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="3.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output omit-xml-declaration="yes" method="text" indent="no"/>
    <xsl:template match="/WAS_WEBAPP_REPORT">name,generation_date,filters_findings_status,filter_remediation,filter_detection_source,target_tags_included,target_tags_excluded,target_web_applications<xsl:text>
</xsl:text>
        <xsl:value-of select="HEADER/NAME"/>,<xsl:value-of select="HEADER/GENERATION_DATETIME"/>,<xsl:apply-templates select="FILTERS/FILTER[NAME='FINDING_STATUS']"/>,<xsl:apply-templates select="FILTERS/FILTER[NAME='REMEDIATION']"/>,<xsl:apply-templates select="FILTERS/FILTER[NAME='DETECTION_SOURCE']"/>,<xsl:apply-templates select="TARGET/TAGS/INCLUDED"/>,<xsl:apply-templates select="TARGET/TAGS/EXCLUDED"/>,<xsl:apply-templates select="TARGET/WEB_APPLICATIONS"/>
    </xsl:template>
    <xsl:template match="FILTERS/FILTER">
       <xsl:variable name="value" select="VALUE"/><xsl:value-of select="translate($value,',',';')"/>
    </xsl:template>
    <xsl:template match="TARGET/TAGS/*">
        <xsl:value-of select="OPTION"/>=<xsl:for-each select="TAG_LIST/TAG"><xsl:value-of select="text()"/> ; </xsl:for-each>
    </xsl:template>
    <xsl:template match="TARGET/WEB_APPLICATIONS">
       <xsl:for-each select="WEB_APPLICATION"><xsl:value-of select="text()"/> ; </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>