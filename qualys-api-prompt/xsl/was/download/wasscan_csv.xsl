<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="3.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                exclude-result-prefixes="xmlns:xsi">
    <xsl:output omit-xml-declaration="yes" method="text" indent="no"/>
    <xsl:template match="/WasScan">id,name,webapp (id),mode,progressiveScanning,launchedDate,endScanDate,scanDuration,status,summary<xsl:text>
</xsl:text>
        <xsl:value-of select="id"/>,<xsl:value-of select="name"/>,<xsl:value-of select="target/webApp/name"/> (<xsl:value-of select="target/webApp/id"/>),<xsl:value-of select="mode"/>,<xsl:value-of select="progressiveScanning"/>,<xsl:value-of select="launchedDate"/>,<xsl:value-of select="endScanDate"/>,<xsl:value-of select="scanDuration"/>,<xsl:value-of select="status"/>,<xsl:apply-templates select="summary"/>
    </xsl:template>
    <xsl:template match="summary">
        <xsl:for-each select="*"><xsl:value-of select="name(.)"/>=<xsl:value-of select="text()"/> ; </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>