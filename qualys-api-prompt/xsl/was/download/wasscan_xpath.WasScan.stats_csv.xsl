<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="3.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                exclude-result-prefixes="xmlns:xsi">
    <xsl:output omit-xml-declaration="yes" method="text" indent="no"/>
    <xsl:template match="/">global,byGroup,byOwasp,byWasc<xsl:text>
</xsl:text>
        <xsl:apply-templates select="/WasScan/stats/global"/>,<xsl:apply-templates select="/WasScan/stats/byGroup/list/*"/>,<xsl:apply-templates select="/WasScan/stats/byOwasp/list/*"/>,<xsl:apply-templates select="/WasScan/stats/byWasc/list/*"/>
    </xsl:template>
    <xsl:template match="/WasScan/stats/global">
        <xsl:for-each select="*"><xsl:value-of select="name(.)"/>=<xsl:value-of select="text()"/> ; </xsl:for-each>
    </xsl:template>
    <xsl:template match="list/GroupStat">
        <xsl:value-of select="group"/> - <xsl:for-each select="node()[starts-with(name(), 'nb')]"><xsl:value-of select="name(.)"/>=<xsl:value-of select="text()"/> ; </xsl:for-each>
    </xsl:template>
    <xsl:template match="list/OwaspStat">
        <xsl:value-of select="owasp"/> - <xsl:for-each select="node()[starts-with(name(), 'nb')]"><xsl:value-of select="name(.)"/>=<xsl:value-of select="text()"/> ; </xsl:for-each>
    </xsl:template>
    <xsl:template match="list/WascStat">
        <xsl:value-of select="wasc"/> - <xsl:for-each select="node()[starts-with(name(), 'nb')]"><xsl:value-of select="name(.)"/>=<xsl:value-of select="text()"/> ; </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>