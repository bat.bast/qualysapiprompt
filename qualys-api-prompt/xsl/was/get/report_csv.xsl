<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="3.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                exclude-result-prefixes="xmlns:xsi">
    <xsl:output omit-xml-declaration="yes" method="text" indent="no"/>
    <xsl:template match="/">id,creationDate,name,status,size,format,type,lastDownloadDate,downloadCount,tags (id)<xsl:text>
</xsl:text>
        <xsl:apply-templates select="/ServiceResponse/data/Report"/>
    </xsl:template>
     <xsl:template match="/ServiceResponse/data/Report">
        <xsl:value-of select="id"/>,<xsl:value-of select="creationDate"/>,<xsl:value-of select="name"/>,<xsl:value-of select="status"/>,<xsl:value-of select="size"/>,<xsl:value-of select="format"/>,<xsl:value-of select="type"/>,<xsl:value-of select="lastDownloadDate"/>,<xsl:value-of select="downloadCount"/>,<xsl:apply-templates select="tags/list"/>
    </xsl:template>
    <xsl:template match="tags/list">
        <xsl:for-each select="Tag"><xsl:value-of select="name"/> (<xsl:value-of select="id"/>) ; </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>