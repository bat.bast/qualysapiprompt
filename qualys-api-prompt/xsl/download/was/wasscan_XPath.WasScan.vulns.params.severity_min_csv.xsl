<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="3.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                exclude-result-prefixes="xmlns:xsi">
    <xsl:output omit-xml-declaration="yes" method="text" indent="no"/>
    <xsl:param name="severity_min">3</xsl:param>
    <xsl:template match="/">type,qid,severity,potential,title,uri<xsl:text>
</xsl:text>
        <xsl:apply-templates select="/WasScan/vulns/list/WasScanVuln[severity >= $severity_min]">
            <xsl:sort select="severity" data-type="number" order="descending"/>
            <xsl:sort select="potential" data-type="text" order="descending"/>
            <xsl:sort select="title" data-type="text" order="ascending"/>
            <xsl:sort select="url" data-type="text" order="ascending"/>
            <xsl:sort select="qid" data-type="number" order="ascending"/>
        </xsl:apply-templates>
        <xsl:apply-templates select="/WasScan/igs/list/WasScanIg[severity >= $severity_min]">
            <xsl:sort select="severity" data-type="number" order="descending"/>
            <xsl:sort select="title" data-type="text" order="ascending"/>
            <xsl:sort select="qid" data-type="number" order="ascending"/>
        </xsl:apply-templates>
    </xsl:template>
    <xsl:template match="/WasScan/vulns/list/WasScanVuln">
vuln,<xsl:value-of select="qid"/>,<xsl:value-of select="severity"/>,<xsl:value-of select="potential"/>,<xsl:value-of select="title"/>,<xsl:value-of select="uri"/>
    </xsl:template>
    <xsl:template match="/WasScan/igs/list/WasScanIg">
ig,<xsl:value-of select="qid"/>,<xsl:value-of select="severity"/>,,<xsl:value-of select="title"/>,
    </xsl:template>
</xsl:stylesheet>