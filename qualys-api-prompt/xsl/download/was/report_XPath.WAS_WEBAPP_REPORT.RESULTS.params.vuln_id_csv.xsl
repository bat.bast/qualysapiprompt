<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="3.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output omit-xml-declaration="yes" method="xml" indent="no"/>
    <xsl:param name="vuln_id">12165334</xsl:param>
    <xsl:key name="glossary_qid" match="/WAS_WEBAPP_REPORT/GLOSSARY/QID_LIST/QID" use="QID"/>
    <xsl:template match="/WAS_WEBAPP_REPORT">webapp_name,severity<xsl:text>
</xsl:text>
        <xsl:apply-templates select="RESULTS/WEB_APPLICATION/VULNERABILITY_LIST/VULNERABILITY[ID = $vuln_id]"/>
        <xsl:apply-templates select="RESULTS/WEB_APPLICATION/INFORMATION_GATHERED_LIST/INFORMATION_GATHERED[ID = $vuln_id]"/>
    </xsl:template>

    <xsl:template match="VULNERABILITY">
        <xsl:value-of select="../../NAME"/><xsl:text>,</xsl:text>
        <xsl:value-of select="key('glossary_qid',QID)/SEVERITY"/><xsl:text>
</xsl:text><xsl:copy-of select="."/>
    </xsl:template>

    <xsl:template match="INFORMATION_GATHERED">
        <xsl:value-of select="../../NAME"/>
        <xsl:value-of select="ID"/>
        <xsl:value-of select="key('glossary_qid',QID)/SEVERITY"/>
        <xsl:text>
</xsl:text>
    </xsl:template>
</xsl:stylesheet>