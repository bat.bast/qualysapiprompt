<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="3.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output omit-xml-declaration="yes" method="text" indent="no"/>
    <xsl:key name="glossary_qid" match="/ASSET_DATA_REPORT/GLOSSARY/VULN_DETAILS_LIST/VULN_DETAILS" use="QID"/>
    <xsl:template match="/ASSET_DATA_REPORT">ip,dns,severity,type,category,title,qid,port,service,protocol,ssl,first_time_detected,last_time_detected,times_found<xsl:text>
</xsl:text>
        <xsl:for-each select="HOST_LIST/HOST/VULN_INFO_LIST/VULN_INFO">
            <xsl:sort select="key('glossary_qid',QID)/SEVERITY" data-type="number" order="descending"/>
            <xsl:sort select="TYPE" data-type="text" order="descending"/>
            <xsl:sort select="key('glossary_qid',QID)/CATEGORY" data-type="text" order="ascending"/>
            <xsl:sort select="../../IP" data-type="number" order="ascending"/>
            <xsl:apply-templates select="."/>
        </xsl:for-each>
    </xsl:template>
    <xsl:template match="VULN_INFO">
        <xsl:if test="key('glossary_qid',QID)/SEVERITY &gt; 3">
            <xsl:value-of select="../../IP"/><xsl:text>,</xsl:text>
            <xsl:value-of select="../../DNS"/><xsl:text>,</xsl:text>
            <xsl:value-of select="key('glossary_qid',QID)/SEVERITY"/><xsl:text>,</xsl:text>
            <xsl:value-of select="TYPE"/><xsl:text>,</xsl:text>
            <xsl:value-of select="key('glossary_qid',QID)/CATEGORY"/><xsl:text>,</xsl:text>
            <xsl:value-of select="key('glossary_qid',QID)/TITLE"/><xsl:text>,</xsl:text>
            <xsl:value-of select="QID"/><xsl:text>,</xsl:text>
            <xsl:value-of select="PORT"/><xsl:text>,</xsl:text>
            <xsl:value-of select="SERVICE"/><xsl:text>,</xsl:text>
            <xsl:value-of select="PROTOCOL"/><xsl:text>,</xsl:text>
            <xsl:value-of select="SSL"/><xsl:text>,</xsl:text>
            <xsl:value-of select="FIRST_FOUND"/><xsl:text>,</xsl:text>
            <xsl:value-of select="LAST_FOUND"/><xsl:text>,</xsl:text>
            <xsl:value-of select="TIMES_FOUND"/><xsl:text>
</xsl:text>
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>