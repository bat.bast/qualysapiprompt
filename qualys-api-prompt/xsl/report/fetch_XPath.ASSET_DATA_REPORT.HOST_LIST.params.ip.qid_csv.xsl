<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="3.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output omit-xml-declaration="yes" method="xml" indent="no"/>
    <xsl:param name="ip">1.2.3.4</xsl:param>
    <xsl:param name="qid">6</xsl:param>
    <xsl:key name="glossary_qid" match="/ASSET_DATA_REPORT/GLOSSARY/VULN_DETAILS_LIST/VULN_DETAILS" use="QID"/>
    <xsl:template match="/ASSET_DATA_REPORT">ip,dns,severity<xsl:text>
</xsl:text>
        <xsl:apply-templates select="HOST_LIST/HOST[IP = $ip]/VULN_INFO_LIST/VULN_INFO[QID = $qid]"/>
    </xsl:template>
    <xsl:template match="VULN_INFO">
        <xsl:value-of select="../../IP"/><xsl:text>,</xsl:text>
        <xsl:value-of select="../../DNS"/><xsl:text>,</xsl:text>
        <xsl:value-of select="key('glossary_qid',QID)/SEVERITY"/><xsl:text>
</xsl:text><xsl:copy-of select="."/>
    </xsl:template>
</xsl:stylesheet>