<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="3.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output omit-xml-declaration="yes" method="text" indent="no"/>
    <xsl:template match="/ASSET_DATA_REPORT">generation_date,vulns,average_security_risk,business_risk,target_tags_scope,target_tags_included<xsl:text>
</xsl:text>
        <xsl:value-of select="HEADER/GENERATION_DATETIME"/>,<xsl:apply-templates select="HEADER/RISK_SCORE_SUMMARY"/>,<xsl:apply-templates select="HEADER/TARGET/ASSET_TAG_LIST/INCLUDED_TAGS"/>
    </xsl:template>
    <xsl:template match="TARGET/ASSET_TAG_LIST/*">
        <xsl:value-of select="@scope"/>,<xsl:for-each select="ASSET_TAG"><xsl:value-of select="text()"/> ; </xsl:for-each>
    </xsl:template>
    <xsl:template match="RISK_SCORE_SUMMARY">
        <xsl:value-of select="TOTAL_VULNERABILITIES"/>,<xsl:value-of select="AVG_SECURITY_RISK"/>,<xsl:value-of select="BUSINESS_RISK"/>
    </xsl:template>
</xsl:stylesheet>