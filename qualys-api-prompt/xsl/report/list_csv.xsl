<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="3.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                exclude-result-prefixes="xmlns:xsi">
    <xsl:output omit-xml-declaration="yes" method="text" indent="no"/>
    <xsl:template match="/">title,id,type,format,size,status,launch,expiration<xsl:text>
</xsl:text>
        <xsl:for-each select="/REPORT_LIST_OUTPUT/RESPONSE/REPORT_LIST/*">
            <xsl:sort select="TITLE" data-type ="text" order="ascending" />
            <xsl:value-of select="TITLE"/><xsl:text>,</xsl:text>
            <xsl:value-of select="ID"/><xsl:text>,</xsl:text>
            <xsl:value-of select="TYPE"/><xsl:text>,</xsl:text>
            <xsl:value-of select="OUTPUT_FORMAT"/><xsl:text>,</xsl:text>
            <xsl:value-of select="SIZE"/><xsl:text>,</xsl:text>
            <xsl:value-of select="STATUS/STATE"/><xsl:text>,</xsl:text>
            <xsl:value-of select="LAUNCH_DATETIME"/><xsl:text>,</xsl:text>
            <xsl:value-of select="EXPIRATION_DATETIME"/><xsl:text>
</xsl:text>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>