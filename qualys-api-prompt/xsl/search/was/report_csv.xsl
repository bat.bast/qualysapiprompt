<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="3.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                exclude-result-prefixes="xmlns:xsi">
    <xsl:output omit-xml-declaration="yes" method="text" indent="no"/>
    <xsl:template match="/">id,creationDate,name,status,size,format,type<xsl:text>
</xsl:text>
        <xsl:for-each select="/ServiceResponse/data/Report">
            <xsl:sort select="creationDate" data-type ="text" order="descending" />
            <xsl:sort select="name" data-type ="text" order="ascending" />
            <xsl:value-of select="id"/>,<xsl:value-of select="creationDate"/>,<xsl:value-of select="name"/>,<xsl:value-of select="status"/>,<xsl:value-of select="size"/>,<xsl:value-of select="format"/>,<xsl:value-of select="type"/><xsl:text>
</xsl:text>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>