<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="3.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                exclude-result-prefixes="xmlns:xsi">
    <xsl:output omit-xml-declaration="yes" method="text" indent="no"/>
    <xsl:template match="/">id,name,url<xsl:text>
</xsl:text>
        <xsl:for-each select="/ServiceResponse/data/WebApp">
            <xsl:sort select="url" data-type ="text" order="ascending" />
            <xsl:value-of select="id"/>,<xsl:value-of select="name"/>,<xsl:value-of select="url"/><xsl:text>
</xsl:text>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>