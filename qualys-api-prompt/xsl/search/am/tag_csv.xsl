<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="3.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                exclude-result-prefixes="xmlns:xsi">
    <xsl:output omit-xml-declaration="yes" method="text" indent="no"/>
    <xsl:template match="/">id,name,parent (id),children (id)<xsl:text>
</xsl:text>
        <xsl:for-each select="/ServiceResponse/data/Tag">
            <xsl:sort select="parentTagId" data-type ="number" order="ascending" />
            <xsl:sort select="name" data-type ="text" order="ascending" />
            <xsl:value-of select="id"/>,<xsl:value-of select="name"/>,<xsl:call-template name="parentTagId"><xsl:with-param
                name="tagId" select="parentTagId"/></xsl:call-template>,<xsl:apply-templates select="children"/><xsl:text>
</xsl:text>
        </xsl:for-each>
    </xsl:template>
    <xsl:template match="children">
        <xsl:for-each select="list/TagSimple"><xsl:value-of select="name"/> (<xsl:value-of select="id"/>) ; </xsl:for-each>
    </xsl:template>
    <xsl:template name="parentTagId">
        <xsl:param name="tagId" />
        <xsl:value-of select="/ServiceResponse/data/Tag[id = $tagId]/name"/>
        <xsl:if test="/ServiceResponse/data/Tag[id = $tagId]/name"> (<xsl:value-of select="/ServiceResponse/data/Tag[id = $tagId]/id"/>)</xsl:if>
    </xsl:template>
</xsl:stylesheet>