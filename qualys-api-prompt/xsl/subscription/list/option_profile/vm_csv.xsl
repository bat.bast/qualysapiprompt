<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="3.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                exclude-result-prefixes="xmlns:xsi">
    <xsl:output omit-xml-declaration="yes" method="text" indent="no"/>
    <xsl:template match="/">id,title,vuln_detection,tcp_type,udp_type,dead_hosts,performance,processes,delay<xsl:text>
</xsl:text>
        <xsl:for-each select="/OPTION_PROFILES/*">
            <xsl:value-of select="BASIC_INFO/ID"/><xsl:text>,</xsl:text>
            <xsl:variable name="value" select="BASIC_INFO/GROUP_NAME"/><xsl:value-of select="translate($value,',',';')"/><xsl:text>,</xsl:text>
            <xsl:choose>
                <xsl:when test="SCAN/VULNERABILITY_DETECTION/COMPLETE">Complete</xsl:when>
                <xsl:otherwise>Partial</xsl:otherwise>
            </xsl:choose><xsl:text>,</xsl:text>
            <xsl:value-of select="SCAN/PORTS/TCP_PORTS/TCP_PORTS_TYPE"/><xsl:text>,</xsl:text>
            <xsl:value-of select="SCAN/PORTS/UDP_PORTS/UDP_PORTS_TYPE"/><xsl:text>,</xsl:text>
            <xsl:value-of select="SCAN/SCAN_DEAD_HOSTS"/><xsl:text>,</xsl:text>

            <xsl:value-of select="SCAN/PERFORMANCE/OVERALL_PERFORMANCE"/><xsl:text>,</xsl:text>
            <xsl:value-of select="SCAN/PERFORMANCE/PROCESSES_TO_RUN/TOTAL_PROCESSES"/><xsl:text>,</xsl:text>
            <xsl:value-of select="SCAN/PERFORMANCE/PACKET_DELAY"/><xsl:text>
</xsl:text>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>