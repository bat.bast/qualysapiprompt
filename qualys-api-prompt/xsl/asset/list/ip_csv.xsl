<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="3.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                exclude-result-prefixes="xmlns:xsi">
    <xsl:output omit-xml-declaration="yes" method="text" indent="no"/>
    <xsl:template match="/">ip,type<xsl:text>
</xsl:text>
        <xsl:for-each select="/IP_LIST_OUTPUT/RESPONSE/IP_SET/*">
            <xsl:sort select="./text()" data-type ="number" order="ascending" />
            <xsl:value-of select="./text()"/>,<xsl:value-of select="name(.)"/><xsl:text>
</xsl:text>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>