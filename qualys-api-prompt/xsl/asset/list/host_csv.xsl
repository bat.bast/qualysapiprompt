<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="3.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                exclude-result-prefixes="xmlns:xsi">
    <xsl:output omit-xml-declaration="yes" method="text" indent="no"/>
    <xsl:template match="/">host,id,tracking_method,dns,os,last_vuln_scan,last_vm_scan,last_vm_scan_duration<xsl:text>
</xsl:text>
        <xsl:for-each select="/HOST_LIST_OUTPUT/RESPONSE/HOST_LIST/*">
            <xsl:sort select="IP" data-type ="number" order="ascending" />
            <xsl:value-of select="IP"/><xsl:text>,</xsl:text>
            <xsl:value-of select="ID"/><xsl:text>,</xsl:text>
            <xsl:value-of select="TRACKING_METHOD"/><xsl:text>,</xsl:text>
            <xsl:value-of select="DNS"/><xsl:text>,</xsl:text>
            <xsl:value-of select="OS"/><xsl:text>,</xsl:text>
            <xsl:value-of select="LAST_VULN_SCAN_DATETIME"/><xsl:text>,</xsl:text>
            <xsl:value-of select="LAST_VM_SCANNED_DATE"/><xsl:text>,</xsl:text>
            <xsl:value-of select="LAST_VM_SCANNED_DURATION"/><xsl:text>
</xsl:text>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>