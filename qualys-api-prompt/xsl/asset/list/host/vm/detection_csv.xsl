<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="3.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                exclude-result-prefixes="xmlns:xsi">
    <xsl:output omit-xml-declaration="yes" method="text" indent="no"/>
    <xsl:template match="/">ip,id,os,dns,qid,severity,status,type,results,first_found_datetime,last_found_date_time,port,protocol,ssl<xsl:text>
</xsl:text>
        <xsl:for-each select="/HOST_LIST_VM_DETECTION_OUTPUT/RESPONSE/HOST_LIST/HOST/DETECTION_LIST/*">
            <xsl:sort select="IP" data-type="text" order="ascending"/>
            <xsl:value-of select="../../IP"/><xsl:text>,</xsl:text>
            <xsl:value-of select="../../ID"/><xsl:text>,</xsl:text>
            <xsl:value-of select="../../OS"/><xsl:text>,</xsl:text>
            <xsl:value-of select="../../DNS"/><xsl:text>,</xsl:text>
            <xsl:value-of select="QID"/><xsl:text>,</xsl:text>
            <xsl:value-of select="SEVERITY"/><xsl:text>,</xsl:text>
            <xsl:value-of select="STATUS"/><xsl:text>,</xsl:text>
            <xsl:value-of select="TYPE"/><xsl:text>,</xsl:text>
            <xsl:value-of select="RESULTS"/><xsl:text>,</xsl:text>
            <xsl:value-of select="FIRST_FOUND_DATETIME"/><xsl:text>,</xsl:text>
            <xsl:value-of select="LAST_FOUND_DATETIME"/><xsl:text>,</xsl:text>
            <xsl:value-of select="PORT"/><xsl:text>,</xsl:text>
            <xsl:value-of select="PROTOCOLE"/><xsl:text>,</xsl:text>
            <xsl:value-of select="TLS"/><xsl:text>
</xsl:text>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>