<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="3.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                exclude-result-prefixes="xmlns:xsi">
    <xsl:output omit-xml-declaration="yes" method="text" indent="no"/>
    <xsl:template match="/">id,name,url,os,tags (id),scanId,scanStatus<xsl:text>
</xsl:text>
        <xsl:apply-templates select="/ServiceResponse/data/WebApp"/>
    </xsl:template>
    <xsl:template match="/ServiceResponse/data/WebApp">
        <xsl:value-of select="id"/>,<xsl:value-of select="name"/>,<xsl:value-of select="url"/>,<xsl:value-of select="os"/>,<xsl:apply-templates select="tags/list"/>,<xsl:value-of select="lastScan/id"/>,<xsl:value-of select="lastScan/summary/resultsStatus"/>
    </xsl:template>
    <xsl:template match="tags/list">
        <xsl:for-each select="Tag"><xsl:value-of select="name"/> (<xsl:value-of select="id"/>) ; </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>