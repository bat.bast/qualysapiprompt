###################################
#          PROMPT COMMANDS        #
###################################
HELP = {
    'help': None,
    'cache': None,
    'history': None,
    'bookmarks': None,
    'debug': None,
    'config': None,
    'format': None,
    'utils': None,
    'query': None,
    'tool': None,
    'verb': None,
    'object': None,
    'condition': None,
    'grep': None,
    'res': None,
    'qualys': None,
}
FORMAT = {
    'CSV': None,
    'XML': None,
}
CONFIG = {
    'qualys': None,
    'prompt': None,
}
UTILS_COMPLEMENT = {
    'encode': {'my_data_to_encode': None,
               '...': None},
    'decode': {'my_data_to_decode': None,
               '...': None}
}
UTILS = {
    'base64': UTILS_COMPLEMENT,
    'url': UTILS_COMPLEMENT
}
DEBUG = {
    'on': None,
    'off': None,
}
CACHE = {
    'on': None,
    'off': None,
    'clear': None,
    'count': None,
    'view': None,
    'expires': {'my delay in format xD_aH:bM:cS': None, '...': None},
}
HISTORY = {
    'clear': None,
    'count': None,
    'view': None,
}
BOOKMARKS = {
    'run': {'my_bookmark_id': None, '...': None},
    'clear': None,
    'count': None,
    'view': None,
    'query': None,
}
QUERY_DICT_PROMPT_COMMANDS = {
    'help': HELP,
    'cache': CACHE,
    'config': CONFIG,
    'debug': DEBUG,
    'history': HISTORY,
    'bookmarks': BOOKMARKS,
    'format': FORMAT,
    'utils': UTILS,
    'grep': {'my_regex': None},
    'res': None,
}

###################################
#              QUALYS             #
###################################
import constants

QUALYS_VALUES = {'My_Value': None,
                 'My_ID': None,
                 'XPath(...)': None,
                 '...': None,
                 'and': {'Add another condition': None}}
QUALYS_COMPARATORS = {comparator: QUALYS_VALUES for comparator in constants.QUALYS_COMPARATORS}
_qualys_comparators = {comparator: QUALYS_VALUES for comparator in
                       [v for v in constants.QUALYS_COMPARATORS.values() if v is not None]}
QUALYS_COMPARATORS.update(_qualys_comparators)
QUALYS_EQUALS_COMPARATOR = {
    'EQUALS': QUALYS_VALUES,
    '=': QUALYS_VALUES,
}
QUALYS_COMPARATORS_DICT = {}
QUALYS_COMPARATORS_DICT['am'] = {'name': QUALYS_COMPARATORS,
                                 'id': QUALYS_COMPARATORS}
QUALYS_COMPARATORS_DICT['scan'] = {'scan_title': QUALYS_EQUALS_COMPARATOR,
                                   'ip': QUALYS_EQUALS_COMPARATOR,
                                   'asset_groups': QUALYS_EQUALS_COMPARATOR,
                                   'asset_group_ids': QUALYS_EQUALS_COMPARATOR,
                                   'tag_include_selector': QUALYS_EQUALS_COMPARATOR,
                                   'tag_exclude_selector': QUALYS_EQUALS_COMPARATOR,
                                   'tag_set_include': QUALYS_EQUALS_COMPARATOR,
                                   'priority': QUALYS_EQUALS_COMPARATOR,
                                   'scan_type': QUALYS_EQUALS_COMPARATOR,
                                   'fqdn': QUALYS_EQUALS_COMPARATOR,
                                   'option_id': QUALYS_EQUALS_COMPARATOR,
                                   'option_title': QUALYS_EQUALS_COMPARATOR,
                                   }
QUALYS_COMPARATORS_DICT['report'] = {'id': QUALYS_EQUALS_COMPARATOR}
QUALYS_COMPARATORS_DICT['asset'] = {'ip': {'ips': QUALYS_EQUALS_COMPARATOR},
                                    'host': {'ips': QUALYS_EQUALS_COMPARATOR,
                                             'ipv6': QUALYS_EQUALS_COMPARATOR,
                                             'details': QUALYS_EQUALS_COMPARATOR,
                                             'ids': QUALYS_EQUALS_COMPARATOR},
                                    'host/vm/detection': {'ids': QUALYS_EQUALS_COMPARATOR,
                                                          'ips': QUALYS_EQUALS_COMPARATOR,
                                                          'ipv6': QUALYS_EQUALS_COMPARATOR,
                                                          'vm_scan_since': QUALYS_EQUALS_COMPARATOR,
                                                          'max_days_since_last_vm_scan': QUALYS_EQUALS_COMPARATOR,
                                                          'status': QUALYS_EQUALS_COMPARATOR,
                                                          'qids': QUALYS_EQUALS_COMPARATOR,
                                                          'severities': QUALYS_EQUALS_COMPARATOR,
                                                          }}
QUALYS_AM = {
    'search': {'MyTagName': None,
               '...': None,
               'tag':
                   {'where': QUALYS_COMPARATORS_DICT['am']}
               },
    'count': {'tag': None},
    'delete': {'tag': {'where': QUALYS_COMPARATORS_DICT['am']}
               }
}
QUALYS_ASSET = {
    'list': {'ip': {'where': QUALYS_COMPARATORS_DICT['asset']['ip']},
             'host': {'where': QUALYS_COMPARATORS_DICT['asset']['host']},
             'host/vm/detection': {'where': QUALYS_COMPARATORS_DICT['asset']['host/vm/detection']}}
}
QUALYS_SUBSCRIPTION = {'list': {'option_profile/vm': None}}
QUALYS_REPORT = {
    'list': {'where': QUALYS_COMPARATORS_DICT['report']},
    'fetch': {'where': QUALYS_COMPARATORS_DICT['report']},
    'delete': {'where': QUALYS_COMPARATORS_DICT['report']},
    'XPath(...)': {'where': QUALYS_COMPARATORS_DICT['report']}
}

QUALYS_SCAN = {
    'launch': {'where': QUALYS_COMPARATORS_DICT['scan']}
}

QUERY_DICT_QUALYS = {
    'am': QUALYS_AM,
    'asset': QUALYS_ASSET,
    'report': QUALYS_REPORT,
    'subscription': QUALYS_SUBSCRIPTION,
    'was': {
        'search': {'webapp':
            {'where':
                {'name':
                    {'CONTAINS': {
                        'My WebApp Partial Name'
                        '...'}
                    }
                }
            }
        },
        'get': {'webapp':
                    {'where':
                         {'id':
                              {'EQUALS': {'my12345679',
                                          'XPath(...)',
                                          '...'}
                               }
                          }
                     }
                }
    }
}
import utils

QUALYS_TOOLS = list(set(utils.extract_dict_level(QUERY_DICT_QUALYS, 0)))
QUALYS_VERBS = list(set(utils.extract_dict_level(QUERY_DICT_QUALYS, 1)))
QUALYS_OBJECTS = list(set(utils.extract_dict_level(QUERY_DICT_QUALYS, 2)))
QUALYS_ATTRIBUTES = list(set(utils.extract_dict_qualys_attributes(QUERY_DICT_QUALYS)))

QUALYS_OBJECTS_ = [
    'tag',
    'wasscan',
    'asset',
    'webapp',
    'report',
    'ip',
    'host',
]

QUALYS_ID_VERBS = [
    'get',
    'download',
    'status',
    'delete',
    'cancel',
    'update',
    'activate',
    'fetch',
]

###################################
#        Final QUERY_DICT         #
###################################
QUERY_DICT = dict(QUERY_DICT_PROMPT_COMMANDS, **QUERY_DICT_QUALYS)

ATTRIBUTES = [
    'id',
    'name',
    'url',
    'isScanned',
    'isScheduled',
    'lastScan.status',
    'lastVulnScan',
    'address',
    'type',
    'tagName',
    'tagId',
    'parentTagId',
    'ruleType',
    'provider',
    'ips',
    'details',
]
