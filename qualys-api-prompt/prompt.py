import argparse
import pathlib

import humanize
import pandas
import re
from datetime import datetime, timedelta

from prompt_toolkit import print_formatted_text, PromptSession
from prompt_toolkit.history import FileHistory, ThreadedHistory
from prompt_toolkit.lexers import PygmentsLexer
from prompt_toolkit.formatted_text import FormattedText, PygmentsTokens
from prompt_toolkit.auto_suggest import AutoSuggestFromHistory
import pygments
from pygments.lexers.html import XmlLexer
from pygments.lexers.sql import SqlLexer

from halo import Halo

import base64
import urllib.parse

import completion
from completer import QualysAPINestedCompleter
from completion import QUERY_DICT_PROMPT_COMMANDS, FORMAT, QUALYS_TOOLS, QUALYS_VERBS, QUALYS_OBJECTS
from qualysapi_client import Client, QueryResult

import version
import constants
import utils


class HistoryData:
    def __init__(self):
        self.history_path = pathlib.Path.home().joinpath(constants.PROGRAM_DIR_ROOT).joinpath(constants.PROGRAM_HISTORY)
        pathlib.Path(self.history_path.parent).mkdir(parents=True, exist_ok=True)
        self.prompt_history = ThreadedHistory(FileHistory(str(self.history_path.absolute())))

    def __str__(self):
        return 'Nb history lines = ' + str(
            self.count())

    def count(self):
        p = pathlib.Path(self.history_path)
        count = 0
        for line in open(p, 'r').readlines():
            if line.startswith('# '):
                count += 1
        return count

    def view(self):
        p = pathlib.Path(self.history_path)
        with open(p, 'r') as p_file:
            return p_file.read()

    def clear(self):
        p = pathlib.Path(self.history_path)
        open(p, 'w').close()


class UtilsData:
    def __init__(self):
        pass

    def base64_encode(self, data: str):
        encodedBytes = base64.b64encode(data.encode("utf-8"))
        return str(encodedBytes, "utf-8")

    def base64_decode(self, data: str):
        decodedBytes = base64.b64decode(data)
        return str(decodedBytes, "utf-8")

    def url_encode(self, data: str):
        return urllib.parse.quote(data)

    def url_decode(self, data: str):
        return urllib.parse.unquote(data)


class GrepData:
    def __init__(self):
        self.grep_pattern = re.compile(r'(.+)\|\s*grep\s+(.+)')

    def analyze_query_grep(self, query: str) -> (str, str):
        if m := self.grep_pattern.match(query):
            return (m.group(1), m.group(2))
        else:
            return (query, None)


class ConfigData:
    def __init__(self, qualys_client: Client):
        self.qualys_client = qualys_client

    def __str__(self):
        return self.config_prompt() + '\n' + self.config_qualys()

    def config_prompt(self):
        return 'Prompt Directory => ' + str(
            pathlib.Path.home().joinpath(constants.PROGRAM_DIR_ROOT).absolute()) + '\n' + \
               'Cache Directory => ' + str(pathlib.Path.home().joinpath(constants.PROGRAM_DIR_ROOT).joinpath(
            constants.PROGRAM_DIR_CACHE).absolute()) + '\n' + \
               'History File => ' + str(pathlib.Path.home().joinpath(constants.PROGRAM_DIR_ROOT).joinpath(
            constants.PROGRAM_HISTORY).absolute()) + '\n' + \
               'Bookmarks File => ' + str(pathlib.Path.home().joinpath(constants.PROGRAM_DIR_ROOT).joinpath(
            constants.PROGRAM_BOOKMARKS).absolute()) + '\n' + \
               'Qualys Configuration File => ' + str(pathlib.Path.home().joinpath(constants.PROGRAM_DIR_ROOT).joinpath(
            constants.PROGRAM_QUALYS_CONFIG).absolute())

    def config_qualys(self):
        return 'Qualys Server => ' + self.qualys_client.qualys_connection.server + '\n' + \
               'Authentication => ' + ' ; '.join(self.qualys_client.qualys_connection.auth) + '\n' + \
               'Proxies => ' + ' ; '.join(
            '{} = {}'.format(key, val) for key, val in self.qualys_client.qualys_connection.proxies.items())


class DebugData:
    def __init__(self):
        self.activated = False

    def __str__(self):
        return 'Activated = ' + str(self.activated)


class BookmarksData:
    def __init__(self):
        self.bookmarks_path = pathlib.Path.home().joinpath(constants.PROGRAM_DIR_ROOT).joinpath(
            constants.PROGRAM_BOOKMARKS)
        pathlib.Path(self.bookmarks_path.parent).mkdir(parents=True, exist_ok=True)
        pathlib.Path(self.bookmarks_path).touch(exist_ok=True)
        self.separator = ';'

    def __str__(self):
        return 'Nb bookmarks = ' + str(
            self.count())

    def count(self):
        p = pathlib.Path(self.bookmarks_path)
        count = 0
        for _ in open(p, 'r').readlines(): count += 1
        return count

    def view(self):
        p = pathlib.Path(self.bookmarks_path)
        try:
            df = pandas.read_csv(p, sep=self.separator, names=['date', 'query'])
            df.index.name = 'index'
            df = df.dropna(how='all')
            if len(df) > 0:
                return df.to_string(index=True, header=True)
        except pandas.errors.EmptyDataError:
            pass
        return 'Bookmarks file is empty'

    def get_query(self, bookmark_id=None):
        if bookmark_id is not None:
            try:
                id = int(bookmark_id)
                if isinstance(id, int):
                    p = pathlib.Path(self.bookmarks_path)
                    df = pandas.read_csv(p, sep=self.separator, names=['date', 'query'])
                    df.index.name = 'index'
                    df = df.dropna(how='all')
                    bookmark_query = None
                    if len(df) > 0:
                        bookmark_query = df.iloc[[id]]['query'][0]
                    return bookmark_query if bookmark_query is not None else None
            except ValueError:
                pass
            except IndexError:
                pass

    def clear(self, bookmark_id=None):
        if bookmark_id is None:
            p = pathlib.Path(self.bookmarks_path)
            open(p, 'w').close()
        else:
            try:
                id = int(bookmark_id)
                if isinstance(id, int):
                    p = pathlib.Path(self.bookmarks_path)
                    df = pandas.read_csv(p, sep=self.separator, names=['date', 'query'])
                    df.index.name = 'index'
                    df = df.dropna(how='all')
                    bookmark = None
                    if len(df) > 0:
                        bookmark = df.iloc[[id]]
                        df = df.drop(id)
                        with open(p, 'w') as p_file:
                            p_file.write(df.to_string(index=False, header=False))
                    return bookmark.to_string(index=False) if bookmark is not None else None
            except ValueError:
                pass
            except IndexError:
                pass

    def add_query(self, query: str):
        if query is not None:
            p = pathlib.Path(self.bookmarks_path)
            with open(p, 'a') as p_file:
                p_file.write(str(datetime.now()) + self.separator + query + '\n')


class CacheData:
    def __init__(self, activated=True, expiry_duration=constants.EXPIRY_DURATION_DEFAULT):
        self.activated = activated
        self.cache_path = pathlib.Path.home().joinpath(constants.PROGRAM_DIR_ROOT).joinpath(constants.PROGRAM_DIR_CACHE)
        pathlib.Path(self.cache_path).mkdir(parents=True, exist_ok=True)
        self.timedelta_expiry_duration, self.expiry_duration = self.set_expiry_duration(expiry_duration)

    def __str__(self):
        return 'Activated = ' + str(
            self.activated) + ', Expiration after = ' + self.expiry_duration + ', Nb Files = ' + str(
            self.count())

    def set_expiry_duration(self, expiry_duration):
        # Expiration duration format xD_aH:bM:cS
        try:
            nb_days = int(expiry_duration[0])
            nb_hours = int(expiry_duration[3])
            nb_minutes = int(expiry_duration[6])
            nb_seconds = int(expiry_duration[9])
            timedelta_expiry_duration = timedelta(days=nb_days, seconds=nb_seconds, minutes=nb_minutes,
                                                  hours=nb_hours)
        except ValueError as e:
            raise e
        return timedelta_expiry_duration, expiry_duration

    def count(self):
        p = pathlib.Path(self.cache_path).glob('**/*')
        files = [x for x in p if x.is_file()]
        return len(files)

    def view(self):
        p = pathlib.Path(self.cache_path).glob('**/*')
        files = [(str(x.name), datetime.fromtimestamp(x.stat().st_mtime).strftime('%Y-%m-%d-%H:%M:%S'),
                  humanize.naturalsize(x.stat().st_size)) for x in p if x.is_file()]
        return files

    def clear(self):
        count = 0
        for x in self.cache_path.iterdir():
            if x.is_file():
                count += 1
                x.unlink(missing_ok=True)
        return count


class QualysAPIPrompt(object):
    def __init__(self, args):
        self.args = args
        self.output_format = 'CSV'
        self.completer = QualysAPINestedCompleter().get_completer()
        self.qualysapi_client = Client()
        self.config = ConfigData(self.qualysapi_client)
        self.cache = CacheData()
        self.grep = GrepData()
        self.utils = UtilsData()
        self.history = HistoryData()
        self.bookmarks = BookmarksData()
        self.debug = DebugData()
        self.previous_query = None
        self.previous_result = None
        self.q_color = {
            'debug': 'ansigray',
            'history': 'ansicyan',
            'cache': 'ansimagenta',
            'config': 'ansibrightcyan',
            'format': 'ansiblue',
            'utils': 'ansidarkblue',
            'bookmarks': 'ansimagenta',
            'help': 'ansiwhite',
            'query': 'ansibrightblue',
            'error': 'ansired',
            'warning': 'ansiyellow',
            'header': 'ansibrightred',
            'data': 'ansibrightgreen',
        }

    def history_query(self, query: str, regex: str):
        words = query.split()
        nb_words = len(words)

        if nb_words == 1:
            print_formatted_text(FormattedText([
                (self.q_color['history'], 'History: ' + str(self.history)),
            ]), end='\n')
        elif nb_words == 2 and utils.query_verify_dict(query, QUERY_DICT_PROMPT_COMMANDS):
            word = words[1].lower()
            if word == 'clear':
                self.history.clear()
                print_formatted_text(FormattedText([
                    (self.q_color['history'], 'History cleared'),
                ]), end='\n')
            elif word == 'count':
                nb = self.history.count()
                print_formatted_text(FormattedText([
                    (self.q_color['history'], 'History: ' + str(nb) + ' lines'),
                ]), end='\n')
            elif word == 'view':
                h = utils.str_match(regex, self.history.view(),)
                print_formatted_text(FormattedText([
                    (self.q_color['history'], 'History details: ' + h),
                ]), end='\n')
            else:
                print_formatted_text(FormattedText([
                    (self.q_color['history'], 'History query not correct'),
                ]), end='\n')
        else:
            print_formatted_text(FormattedText([
                (self.q_color['history'], 'History query not correct'),
            ]), end='\n')

    def bookmarks_query(self, query):
        words = query.split()
        nb_words = len(words)

        if nb_words == 1:
            print_formatted_text(FormattedText([
                (self.q_color['bookmarks'], 'Bookmarks: ' + str(self.bookmarks)),
            ]), end='\n')
        elif nb_words == 2 and utils.query_verify_dict(query, QUERY_DICT_PROMPT_COMMANDS):
            word = words[1].lower()
            if word == 'clear':
                self.bookmarks.clear()
                print_formatted_text(FormattedText([
                    (self.q_color['bookmarks'], 'Bookmarks cleared'),
                ]), end='\n')
            elif word == 'count':
                nb = self.bookmarks.count()
                print_formatted_text(FormattedText([
                    (self.q_color['bookmarks'], 'Bookmarks: ' + str(nb) + ' records'),
                ]), end='\n')
            elif word == 'view':
                h = self.bookmarks.view()
                print_formatted_text(FormattedText([
                    (self.q_color['bookmarks'], 'Bookmarks details: \n' + h),
                ]), end='\n')
            elif word == 'query':
                self.bookmarks.add_query(self.previous_query)
                print_formatted_text(FormattedText([
                    (self.q_color['bookmarks'], 'Query "' + str(self.previous_query) + '" added to bookmarks'),
                ]), end='\n')
            else:
                print_formatted_text(FormattedText([
                    (self.q_color['bookmarks'], 'Bookmarks query not correct'),
                ]), end='\n')
        elif nb_words == 3 and words[1].lower() == 'clear':
            word = words[2]
            bookmark_deleted = self.bookmarks.clear(word)
            print_formatted_text(FormattedText([
                (self.q_color['bookmarks'], 'Bookmark #' + word + ' deleted --> ' + str(bookmark_deleted)),
            ]), end='\n')
        elif nb_words == 3 and words[1].lower() == 'run':
            word = words[2]
            bookmark_query = self.bookmarks.get_query(word)
            self._execute_query(bookmark_query)
        else:
            print_formatted_text(FormattedText([
                (self.q_color['bookmarks'], 'Bookmarks query not correct'),
            ]), end='\n')

    def utils_query(self, query):
        words = query.split()
        nb_words = len(words)

        if nb_words == 4 and utils.query_verify_dict(query, QUERY_DICT_PROMPT_COMMANDS):
            algo = words[1].lower()
            action = words[2].lower()
            data = words[3]
            do_algo_action = getattr(self.utils, algo + '_' + action)
            res = do_algo_action(data)
            print_formatted_text(FormattedText([
                (self.q_color['utils'], 'Utils: ' + res),
            ]), end='\n')
        else:
            print_formatted_text(FormattedText([
                (self.q_color['utils'], 'Utils query not correct'),
            ]), end='\n')

    def format_query(self, query):
        words = query.split()
        nb_words = len(words)

        if nb_words == 2 and utils.query_verify_dict(query, QUERY_DICT_PROMPT_COMMANDS):
            print_formatted_text(FormattedText([
                (self.q_color['format'], 'New Output Format: ' + words[1]),
            ]), end='\n')
            self.output_format = words[1]
        else:
            print_formatted_text(FormattedText([
                (self.q_color['format'], 'Current Output Format: ' + self.output_format),
            ]), end='\n')

    def debug_query(self, query):
        words = query.split()
        nb_words = len(words)

        if nb_words == 2 and utils.query_verify_dict(query, QUERY_DICT_PROMPT_COMMANDS):
            word = words[1].lower()
            if word == 'on':
                print_formatted_text(FormattedText([
                    (self.q_color['debug'], 'Debug activation: ' + str(self.debug.activated) + ' --> True'),
                ]), end='\n')
                self.debug.activated = True
            elif word == 'off':
                print_formatted_text(FormattedText([
                    (self.q_color['debug'], 'Debug activation: ' + str(self.debug.activated) + ' --> False'),
                ]), end='\n')
                self.debug.activated = False
            else:
                print_formatted_text(FormattedText([
                    (self.q_color['debug'], 'Current Debug status: ' + str(self.debug)),
                ]), end='\n')
        else:
            print_formatted_text(FormattedText([
                (self.q_color['debug'], 'Current Debug status: ' + str(self.debug)),
            ]), end='\n')

    def cache_query(self, query):
        words = query.split()
        nb_words = len(words)

        if nb_words == 1:
            print_formatted_text(FormattedText([
                (self.q_color['cache'], 'Cache: ' + str(self.cache)),
            ]), end='\n')
        elif nb_words == 2 and utils.query_verify_dict(query, QUERY_DICT_PROMPT_COMMANDS):
            word = words[1].lower()
            if word == 'on':
                print_formatted_text(FormattedText([
                    (self.q_color['cache'], 'Cache activation: ' + str(self.cache.activated) + ' --> True'),
                ]), end='\n')
                self.cache.activated = True
            elif word == 'off':
                print_formatted_text(FormattedText([
                    (self.q_color['cache'], 'Cache activation: ' + str(self.cache.activated) + ' --> False'),
                ]), end='\n')
                self.cache.activated = False
            elif word == 'clear':
                nb = self.cache.clear()
                print_formatted_text(FormattedText([
                    (self.q_color['cache'], 'Cache cleared: ' + str(nb) + ' files deleted'),
                ]), end='\n')
            elif word == 'count':
                nb = self.cache.count()
                print_formatted_text(FormattedText([
                    (self.q_color['cache'], 'Cache files: ' + str(nb) + ' files'),
                ]), end='\n')
            elif word == 'view':
                files = self.cache.view()
                print_formatted_text(FormattedText([
                    (self.q_color['cache'], 'Cache files details: ' + str(len(files)) + ' files'),
                ]), end='\n')
                for n, d, s in files:
                    print_formatted_text(FormattedText([
                        (self.q_color['cache'], n + ' - ' + d + ' - ' + s),
                    ]), end='\n')
            else:
                print_formatted_text(FormattedText([
                    (self.q_color['cache'], 'Cache query not correct'),
                ]), end='\n')
        elif nb_words == 3 and words[1].lower() == 'expires':
            word = words[2].upper()
            try:
                self.cache.timedelta_expiry_duration, self.cache.expiry_duration = self.cache.set_expiry_duration(word)
            except ValueError as e:
                self.cache.timedelta_expiry_duration, self.cache.expiry_duration = self.cache.set_expiry_duration(
                    constants.EXPIRY_DURATION_DEFAULT)
                word = 'Error into "' + word + '" ==> ' + constants.EXPIRY_DURATION_DEFAULT
            print_formatted_text(FormattedText([
                (self.q_color['cache'], 'Cache expiration: ' + self.cache.expiry_duration + ' --> ' + word),
            ]), end='\n')
        else:
            print_formatted_text(FormattedText([
                (self.q_color['cache'], 'Cache query not correct'),
            ]), end='\n')

    def config_query(self, query):
        words = query.split()
        nb_words = len(words)

        if nb_words == 1:
            print_formatted_text(FormattedText([
                (self.q_color['config'], 'Configuration:\n' + str(self.config)),
            ]), end='\n')
        elif nb_words == 2 and utils.query_verify_dict(query, QUERY_DICT_PROMPT_COMMANDS):
            word = words[1].lower()
            if word == 'qualys':
                print_formatted_text(FormattedText([
                    (self.q_color['config'], 'Qualys Configuration:\n' + str(self.config.config_qualys())), ]),
                    end='\n')
            elif word == 'prompt':
                print_formatted_text(FormattedText([
                    (self.q_color['config'], 'Prompt Configuration:\n' + str(self.config.config_prompt())), ]),
                    end='\n')
            else:
                print_formatted_text(FormattedText([
                    (self.q_color['config'], constants.HELP_QUERY_CONFIG),
                ]), end='\n')
        else:
            print_formatted_text(FormattedText([
                (self.q_color['config'], constants.HELP_QUERY_CONFIG),
            ]), end='\n')

    def help_query(self, query):
        words = query.split()
        nb_words = len(words)

        if nb_words == 2 and utils.query_verify_dict(query, QUERY_DICT_PROMPT_COMMANDS):
            help_term = words[1].lower()
            if help_term == 'cache':
                print_formatted_text(FormattedText([
                    (self.q_color['help'], constants.HELP_QUERY_CACHE),
                ]), end='\n')
            elif help_term == 'history':
                print_formatted_text(FormattedText([
                    (self.q_color['help'], constants.HELP_QUERY_HISTORY),
                ]), end='\n')
            elif help_term == 'utils':
                print_formatted_text(FormattedText([
                    (self.q_color['help'], constants.HELP_QUERY_UTILS),
                ]), end='\n')
            elif help_term == 'debug':
                print_formatted_text(FormattedText([
                    (self.q_color['help'], constants.HELP_QUERY_DEBUG),
                ]), end='\n')
            elif help_term == 'config':
                print_formatted_text(FormattedText([
                    (self.q_color['config'], constants.HELP_QUERY_CONFIG),
                ]), end='\n')
            elif help_term == 'bookmarks':
                print_formatted_text(FormattedText([
                    (self.q_color['help'], constants.HELP_QUERY_BOOKMARKS),
                ]), end='\n')
            elif help_term == 'grep':
                print_formatted_text(FormattedText([
                    (self.q_color['help'], constants.HELP_QUERY_GREP),
                ]), end='\n')
            elif help_term == 'res':
                print_formatted_text(FormattedText([
                    (self.q_color['help'], constants.HELP_QUERY_RES),
                ]), end='\n')
            elif help_term == 'qualys':
                for key in constants.HELP_QUALYS_URL:
                    print_formatted_text(FormattedText([
                        (self.q_color['help'], key + ' --> ' + constants.HELP_QUALYS_URL[key]),
                    ]), end='\n')
            elif help_term == 'query':
                print_formatted_text(FormattedText([
                    (self.q_color['help'], constants.HELP_QUERY_FORMAT),
                ]), end='\n')
            elif help_term == 'tool':
                print_formatted_text(FormattedText([
                    (self.q_color['help'], 'List of tool keywords:'),
                ]), end='\n')
                for tool in QUALYS_TOOLS:
                    print_formatted_text(FormattedText([
                        (self.q_color['help'], '\t' + tool),
                    ]), end='\n')
            elif help_term == 'verb':
                print_formatted_text(FormattedText([
                    (self.q_color['help'], 'List of verb keywords:'),
                ]), end='\n')
                for verb in QUALYS_VERBS:
                    print_formatted_text(FormattedText([
                        (self.q_color['help'], '\t' + verb),
                    ]), end='\n')
            elif help_term == 'object':
                print_formatted_text(FormattedText([
                    (self.q_color['help'], 'List of object keywords:'),
                ]), end='\n')
                for obj in QUALYS_OBJECTS:
                    print_formatted_text(FormattedText([
                        (self.q_color['help'], '\t' + obj),
                    ]), end='\n')
            elif help_term == 'condition':
                print_formatted_text(FormattedText([
                    (self.q_color['help'], constants.HELP_QUALYS_QUERY_CONDITION_FORMAT),
                ]), end='\n')
                print_formatted_text(FormattedText([
                    (self.q_color['help'], 'List of attribute keywords:'),
                ]), end='\n')
                for attribute in completion.QUALYS_ATTRIBUTES:
                    print_formatted_text(FormattedText([
                        (self.q_color['help'], '\t' + attribute),
                    ]), end='\n')
                print_formatted_text(FormattedText([
                    (self.q_color['help'], 'List of comparator keywords:'),
                ]), end='\n')
                for comparator in constants.QUALYS_COMPARATORS:
                    print_formatted_text(FormattedText([
                        (self.q_color['help'], '\t' + comparator),
                    ]), end='\n')
            elif help_term == 'format':
                print_formatted_text(FormattedText([
                    (self.q_color['help'], 'List of output format keywords:'),
                ]), end='\n')
                for attribute in FORMAT:
                    print_formatted_text(FormattedText([
                        (self.q_color['help'], '\t' + attribute),
                    ]), end='\n')
            else:
                print_formatted_text(FormattedText([
                    (self.q_color['help'], constants.HELP_QUERY_HELP),
                ]), end='\n')
        else:
            print_formatted_text(FormattedText([
                (self.q_color['help'], constants.HELP_QUERY_HELP),
            ]), end='\n')

    def run_cli(self):
        print_formatted_text(FormattedText([
            ('grey', '{0} - Version: {1}'.format(version.__name__, version.__version__))
        ]), end='\n')
        print_formatted_text(FormattedText([
            ('ansibrightred', 'W'),
            ('orange', 'e'),
            ('ansibrightyellow', 'l'),
            ('ansired', 'c'),
            ('blue', 'o'),
            ('indigo', 'm'),
            ('purple', 'e'),
            ('yellow', '! ')
        ]), end='\n')
        print_formatted_text(FormattedText([
            (self.q_color['help'], 'For help: type help')
        ]), end='\n')

        session = PromptSession(
            lexer=PygmentsLexer(SqlLexer),  # Our DSL is near SQL language
            search_ignore_case=True,
            complete_while_typing=True,
            enable_history_search=False,  # If True, completer is not operational
            history=self.history.prompt_history,
            completer=self.completer,
            auto_suggest=AutoSuggestFromHistory(), )

        prompt_text = '{0}> '.format('QualysAPI')

        while True:
            try:
                query = session.prompt(prompt_text)
            except KeyboardInterrupt:
                continue
            except EOFError:
                print('Goodbye!')
                return

            query = query.strip()
            if query == '':
                continue

            # Parse and execute query
            self._execute_query(query)

            # Query is executed, this is now a previous query
            self.previous_query = query

    def _print_errors_query(self, query_result: QueryResult):
        if not query_result.without_errors():
            for error_id, error_msg in query_result.errors:
                print_formatted_text(
                    FormattedText([(self.q_color['error'], '[ERROR][' + error_id + '] ' + error_msg), ]), end='\n')

    def _print_warnings_query(self, query_result: QueryResult):
        if not query_result.without_warnings():
            for warning_id, warning_msg in query_result.warnings:
                print_formatted_text(
                    FormattedText([(self.q_color['warning'], '[WARNING][' + warning_id + '] ' + warning_msg), ]),
                    end='\n')

    def _print_query(self, query_result: QueryResult):
        if query_result.query_transformed == query_result.query_original:
            query_text = 'Query: "' + str(query_result.query_original) + '"'
            print_formatted_text(FormattedText([(self.q_color['query'], query_text), ]), end='\n')
        else:
            query_text = 'Query: Updated from "' + str(query_result.query_original) + '" --> "' + str(
                query_result.query_transformed) + '"'
            print_formatted_text(FormattedText([(self.q_color['query'], query_text), ]), end='\n')

    def _print_query_result(self, query_result: QueryResult):
        if query_result.result_transformed is not None:
            if query_result.transformed_ouput_format == 'XML':
                # Print Qualys XML result
                tokens = list(pygments.lex(query_result.result_transformed, lexer=XmlLexer()))
                print_formatted_text(PygmentsTokens(tokens))
            elif query_result.transformed_ouput_format == 'CSV':
                print_formatted_text(FormattedText(
                    utils.result_csv_to_tabular(query_result.result_transformed, self.q_color['header'],
                                                self.q_color['data'], self.q_color['error'], self.debug.activated)))
            elif query_result.transformed_ouput_format == 'PDF':
                print_formatted_text(FormattedText((self.q_color['data'], query_result.result_transformed + '\n')))
            else:
                print_formatted_text(FormattedText((self.q_color['data'], query_result.result_pure + '\n')))

    def _print_debug_query(self, query_result: QueryResult):
        if self.debug.activated:
            self._print_warnings_query(query_result)
            query_text = 'Original Query: ' + query_result.query_original
            print_formatted_text(FormattedText([
                (self.q_color['debug'], query_text), ]), end='\n')
            print_formatted_text(FormattedText([
                (self.q_color['debug'],
                 'Qualys request: ' + str(query_result.qualys_call) + ' + ' + str(query_result.qualys_parameters)), ]),
                end='\n')
            if query_result.xpath is not None:
                print_formatted_text(FormattedText([
                    (self.q_color['debug'], 'XPath : ' + str(query_result.xpath)), ]), end='\n')
            print_formatted_text(FormattedText([
                (self.q_color['debug'], 'Cache used: ' + str(query_result.cache_used)), ]), end='\n')
            print_formatted_text(FormattedText([
                (self.q_color['debug'], 'Cache expiration: ' + str(query_result.cache_expiration)), ]), end='\n')
            print_formatted_text(FormattedText([
                (self.q_color['debug'], 'Cache file: ' + str(query_result.query_cache_file_path)), ]), end='\n')
            print_formatted_text(FormattedText([
                (self.q_color['debug'], 'Cache file: ' + str(query_result.query_cache_file_mtime)), ]), end='\n')
            print_formatted_text(FormattedText([
                (self.q_color['debug'], 'Cache file error: ' + str(query_result.query_cache_file_path_error)), ]),
                end='\n')
            print_formatted_text(FormattedText([
                (self.q_color['debug'], 'Output format: ' + str(query_result.transformed_ouput_format)), ]), end='\n')
            print_formatted_text(FormattedText([
                (self.q_color['debug'], 'XSL file: ' + str(query_result.xsl_file)), ]), end='\n')
            print_formatted_text(FormattedText([
                (self.q_color['debug'], 'XSL Parameters: ' + str(query_result.xsl_file_xpath_params)), ]), end='\n')

    def _execute_query(self, query: str):
        if query is not None:
            query, regex = self.grep.analyze_query_grep(query)
            if query.startswith('help'):
                self.help_query(query)
            elif query.startswith('format'):
                self.format_query(query)
            elif query.startswith('utils'):
                self.utils_query(query)
            elif query.startswith('debug'):
                self.debug_query(query)
            elif query.startswith('cache'):
                self.cache_query(query)
            elif query.startswith('config'):
                self.config_query(query)
            elif query.startswith('history'):
                self.history_query(query, regex)
            elif query.startswith('res', regex):
                self.res_query(query)
            elif query.startswith('bookmarks'):
                self.bookmarks_query(query, regex)
            else:
                self._execute_qualys_query(query, regex)

    def _execute_qualys_query(self, query: str):
        previous_results = {}
        index = 0
        for q in query.split('|'):
            clean_q = q.strip()
            spinner = Halo(text='Loading Qualys data for query "' + clean_q + '"', spinner='dots2')
            spinner.start()

            query_result = QueryResult()
            query_result.transformed_ouput_format = self.output_format
            query_result.cache_activated = self.cache.activated
            query_result.cache_expiration = self.cache.timedelta_expiry_duration
            query_result.cache_dir = self.cache.cache_path
            query_result.query_original = clean_q

            # Call Qualys API
            self.qualysapi_client.query(query_result, previous_results)

            # Save result XML for the next query
            previous_results[index] = query_result.result_pure
            index += 1

            spinner.stop()

            # Print results
            self._print_query(query_result)
            self._print_debug_query(query_result)
            self._print_errors_query(query_result)
            self._print_query_result(query_result)

        # Clear variable
        del previous_results


def cli():
    parser = argparse.ArgumentParser(add_help=False)
    parser.add_argument(
        "--help", action="help",
        help="show this help message and exit")
    parser.add_argument(
        '-v', '--version',
        action='version', version=version.__version__)
    args = parser.parse_args()

    qualysapi_prompt = QualysAPIPrompt(vars(args))
    qualysapi_prompt.run_cli()


if __name__ == "__main__":
    cli()
