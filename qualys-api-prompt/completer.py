from prompt_toolkit.completion import NestedCompleter

from completion import QUERY_DICT


class QualysAPINestedCompleter:
    def __init__(self):
        self._completer = None

    def get_completer(self) -> NestedCompleter:
        if self._completer is None:
            self._completer = NestedCompleter.from_nested_dict(QUERY_DICT)
        return self._completer
