HELP_QUERY_HELP = """help qualys ---------> Help about Qualys
help query ----------> Help about Qualys query format
help tool -----------> Help about Qualys tool word into the query
help verb -----------> Help about Qualys verb word into the query
help object ---------> Help about Qualys object word into the query
help condition ------> Help about Qualys condition words into the query
help format ---------> Help about result output format
help utils ----------> Help about utils functions
help cache ----------> Help about cache configuration
help history --------> Help about history
help bookmarks ------> Help about cache bookmarks
help debug ----------> Help about debug
help config ---------> Help about configuration
help grep -----------> Help about grep
help res ------------> Help about res
"""
HELP_QUALYS_QUERY_CONDITION_FORMAT = """Condition format: attribute comparator criteria
        You can put several conditions with keyword 'and'"""

HELP_QUERY_FORMAT = """Simple Query format: tool verb object [XPath(...)] [where condition]
XPath is used to select correct XSL file
    You can pass parameters to XSL file with XPath(/my/path, param1=val1, param2=val2...)
Chained Queries: tool verb object [where condition]
    | tool verb object where attribute comparator XPath($0/ServiceResponse/...])
    [| tool verb object where attribute comparator XPath($1/ServiceResponse/...)]"""

HELP_QUALYS_URL = {'API Quick Reference': 'https://www.qualys.com/docs/qualys-api-quick-reference.pdf',
                   'Asset Management': 'https://www.qualys.com/docs/qualys-asset-management-tagging-api-v2-user-guide.pdf',
                   'Vulnerability Management - Policy Compliance': 'https://www.qualys.com/docs/qualys-api-vmpc-user-guide.pdf',
                   'Web Application Scanning': 'https://www.qualys.com/docs/qualys-was-api-user-guide.pdf'}

HELP_QUERY_CACHE = """Cache: cache [on|off|clear|view|expires <duration with format=xD_aH:bM:cS>]"""

HELP_QUERY_UTILS = """Utils: utils <base64|url> <encode|decode> <my_data>"""

HELP_QUERY_HISTORY = """History: history [clear|view]"""

HELP_QUERY_BOOKMARKS = """Bookmarks: bookmarks [run <id bookmark>|clear|view|query (bookmark previous query)]"""

HELP_QUERY_DEBUG = """Debug: debug [on|off]"""

HELP_QUERY_GREP = """Grep: grep my_regex"""

HELP_QUERY_RES = """Previous result: res is the result of the previous query"""

HELP_QUERY_CONFIG = """Configuration: config [qualys|prompt]"""

XSL_DEFAULT_FILE = 'default'

PROGRAM_DIR_ROOT = '.qualys_api_prompt'
PROGRAM_DIR_CACHE = 'cache'
PROGRAM_HISTORY = 'history'
PROGRAM_BOOKMARKS = 'bookmarks'
PROGRAM_QUALYS_CONFIG = 'qualys_config'

EXPIRY_DURATION_DEFAULT = '6D_0H:0M:0S'

QUALYS_API_VERSION = {
    'am': '3.0',
    'was': '3.0',
    'asset': '2.0',
    'report': '2.0',
    'scan': '2.0',
    'subscription': '2.0',
    'vm': '2.0',
}
QUALYS_COMPARATORS = {
    'EQUALS': '=',
    'NOT_EQUALS': '!=',
    'GREATER': '>',
    'LESSER': '<',
    'IN': None,
    'CONTAINS': None}
QUALYS_FILTERS_LIST = ['where']
CONJUCTIONS_LIST = ['and']

EXPECTED_XML_RESULTS = ['<responseCode>SUCCESS</responseCode>',
                        '<WasScan xmlns:xsi=',
                        '<WAS_WEBAPP_REPORT>',
                        '<ASSET_DATA_REPORT>',
                        '<HOST_LIST_VM_DETECTION_OUTPUT>',
                        'LIST_OUTPUT>',
                        '<OPTION_PROFILES>']
