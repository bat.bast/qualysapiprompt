# Qualys API Prompt
Manipulate Qualys API with a DSL.

Inspired from https://github.com/RPing/influx-prompt

## Usage
Search WAS application
> was search webapp where name CONTAINS my.site.org

Show WAS application details
> was get webapp where id EQUALS 12345679

Show vunerabilities for an application
> was search webapp where name CONTAINS my.site.org

## Requirements
python 3.9.x  
prompt_toolkit  
pygments  
qualysapi

## Content

## Issues
Please report any bugs or requests that you have using this Gitlab repo.

## Contributing
Bat Bast
