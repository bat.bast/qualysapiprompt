import unittest


class TestQuery(unittest.TestCase):

    def test_configuration(self):
        from datetime import timedelta
        from query_result import QueryResult
        query_result = QueryResult()

        assert query_result.cache_expiration > timedelta(days=0)

    def test_simple_query(self):
        from qualysapi_client import Client
        from query_result import QueryResult
        qualysapi_client = Client()
        query_result = QueryResult()
        query_result.query_original = 'am search tag'

        qualysapi_client._make_call_args(query_result)

        assert query_result.query_original == 'am search tag'
        assert query_result.query_transformed == 'am search tag'
        assert query_result.without_errors() is True
        assert query_result.qualys_parameters is None
        assert query_result.qualys_call == '/search/am/tag'

    def test_error_query(self):
        from qualysapi_client import Client
        from query_result import QueryResult
        qualysapi_client = Client()
        query_result = QueryResult()
        query_result.query_original = 'am search tagXXX'

        qualysapi_client._make_call_args(query_result)

        assert query_result.without_errors() is False

    def test_simple_filter_query(self):
        from qualysapi_client import Client
        from query_result import QueryResult
        qualysapi_client = Client()
        query_result = QueryResult()
        query_result.query_original = 'am search tag where name EQUALS BU_BAT'

        qualysapi_client._make_call_args(query_result)

        assert query_result.without_errors() is True
        assert query_result.qualys_parameters.find('EQUALS') > 0
        assert query_result.qualys_call == '/search/am/tag'

    def test_double_filters_query(self):
        from qualysapi_client import Client
        from query_result import QueryResult
        qualysapi_client = Client()
        query_result = QueryResult()
        query_result.query_original = 'am search tag where name EQUALS BU_BAT and name CONTAINS BAT'

        qualysapi_client._make_call_args(query_result)

        assert query_result.without_errors() is True
        assert query_result.qualys_parameters.find('BU_BAT') < query_result.qualys_parameters.find('BAT')
        assert query_result.qualys_call == '/search/am/tag'

    def test_query_transformed(self):
        from qualysapi_client import Client
        from query_result import QueryResult
        qualysapi_client = Client()
        query_result = QueryResult()
        query_result.query_original = 'Am sEArch tag XPath(/toot/titi,param1=val1,PARAM2=VAL2) WHERE name EQUALS BU_BAT aNd name CONTAINS BAT'

        qualysapi_client._make_call_args(query_result)

        assert query_result.without_errors() is True
        assert query_result.qualys_call == '/search/am/tag'
        assert query_result.query_transformed == 'am search tag XPath(/toot/titi,param1=val1,PARAM2=VAL2) where name EQUALS BU_BAT and name CONTAINS BAT'

    def test_query_transformed_comparator(self):
        from qualysapi_client import Client
        from query_result import QueryResult
        qualysapi_client = Client()
        query_result = QueryResult()
        query_result.query_original = 'am search tag where name = BU_BAT and id > 3 and name NOT_EQUALS bad_name and name !=  another_bad_name'

        qualysapi_client._make_call_args(query_result)

        assert query_result.without_errors() is True
        assert query_result.qualys_parameters == '<ServiceRequest><filters><Criteria field="name" operator="EQUALS">BU_BAT</Criteria><Criteria field="id" operator="GREATER">3</Criteria><Criteria field="name" operator="NOT EQUALS">bad_name</Criteria><Criteria field="name" operator="NOT EQUALS">another_bad_name</Criteria></filters></ServiceRequest>'

    def test_qualysapi_invalid_criteria_query(self):
        from qualysapi_client import Client
        from query_result import QueryResult
        qualysapi_client = Client()
        query_result = QueryResult()
        query_result.query_original = 'am search tag where tagNameXXX CONTAINS BU_BAT'
        query_result.transformed_ouput_format = 'XML'

        qualysapi_client._make_call_args(query_result)

        assert query_result.without_errors() is False
        assert query_result.errors[0][1].find('tagNameXXX') > 0

    def test_qualysapi_api_tag_not_exists(self):
        from qualysapi_client import Client
        from query_result import QueryResult
        qualysapi_client = Client()
        query_result = QueryResult()
        query_result.query_original = 'am search tag where name CONTAINS BU_DOES_NOT_EXIST_XXXXXXXX'
        query_result.transformed_ouput_format = 'CSV'

        qualysapi_client.query(query_result)

        assert query_result.without_errors() is True
        assert query_result.result_transformed.find('BU_DOES_NOT_EXIST_XXXXXXXX') < 0
        assert query_result.cache_activated is True

    def test_qualysapi_api_tag_exists(self):
        from qualysapi_client import Client
        from query_result import QueryResult
        qualysapi_client = Client()
        query_result = QueryResult()
        query_result.query_original = 'am search tag where name CONTAINS BU_BAT'
        query_result.transformed_ouput_format = 'CSV'

        qualysapi_client.query(query_result)

        assert query_result.without_errors() is True
        assert query_result.result_transformed.find('BU_BAT') > 0
        assert query_result.cache_activated is True

    def test_xpath_extract(self):
        import utils

        xp0 = 'XPath(/0/1/2_malformed'
        xpath, params = utils.extract_xpath_data(xp0)
        assert xpath is None
        assert params is None

        xp1 = 'XPath(/0/1/2)'
        xpath, params = utils.extract_xpath_data(xp1)
        assert xpath == '/0/1/2'
        assert len(params) == 0

        xp2 = 'XPath(/0/1/2,param0=0, param1=str1,      2      =      str2 with 3 spaces      )'
        xpath, params = utils.extract_xpath_data(xp2)
        assert xpath == '/0/1/2'
        assert len(params) == 3
        assert params['param1'] == "'str1'"
        assert params['2'].count(' ') == 3

    def test_qualysapi_asset_ip_simple(self):
        from qualysapi_client import Client
        from query_result import QueryResult
        qualysapi_client = Client()
        query_result = QueryResult()
        query_result.query_original = 'asset list ip'
        query_result.transformed_ouput_format = 'CSV'

        qualysapi_client.query(query_result)

        assert query_result.without_errors() is True

    def test_qualysapi_asset_ip(self):
        from qualysapi_client import Client
        from query_result import QueryResult
        qualysapi_client = Client()
        query_result = QueryResult()
        query_result.query_original = 'asset list ip where ips EQUALS 192.0.0.0-192.255.255.255'
        query_result.transformed_ouput_format = 'CSV'

        qualysapi_client.query(query_result)

        assert query_result.without_errors() is True
        assert query_result.result_transformed.find('192.') > 0

    def test_qualysapi_asset_host(self):
        from qualysapi_client import Client
        from query_result import QueryResult
        qualysapi_client = Client()
        query_result = QueryResult()
        query_result.query_original = 'asset list host where details EQUALS All'
        query_result.transformed_ouput_format = 'CSV'

        qualysapi_client.query(query_result)

        assert query_result.without_errors() is True
        assert query_result.result_transformed.find('192.') > 0

    def test_qualysapi_report_list(self):
        from qualysapi_client import Client
        from query_result import QueryResult
        qualysapi_client = Client()
        query_result = QueryResult()
        query_result.query_original = 'report list'
        query_result.transformed_ouput_format = 'CSV'

        qualysapi_client.query(query_result)

        assert query_result.without_errors() is True
        assert query_result.result_transformed.find('Finished') > 0

    def test_qualysapi_vm_detection_list(self):
        from qualysapi_client import Client
        from query_result import QueryResult
        qualysapi_client = Client()
        query_result = QueryResult()
        query_result.query_original = 'asset list host/vm/detection'
        query_result.transformed_ouput_format = 'CSV'

        qualysapi_client.query(query_result)

        assert query_result.without_errors() is True
        assert query_result.result_transformed.find('|first_found_datetime|') > 0

        query_result = QueryResult()
        query_result.query_original = 'asset list host/vm/detection where ips EQUALS 202.22.156.90'
        query_result.transformed_ouput_format = 'CSV'

        qualysapi_client.query(query_result)

        assert query_result.without_errors() is True
        assert query_result.result_transformed.find('|first_found_datetime|') > 0

    def test_query_dict(self):
        import utils
        from completion import QUERY_DICT, QUALYS_COMPARATORS_DICT
        my_dict = QUERY_DICT

        assert utils.query_verify_dict('asset list ip', my_dict) is True

        assert utils.query_verify_dict('am search tagXXX', my_dict) is True
        assert utils.query_verify_dict('am search tag where name EQUALS BU_BAT and id CONTAINS BAT', my_dict,
                                       QUALYS_COMPARATORS_DICT['am']) is True
        assert utils.query_verify_dict('am search tag where name EQUALS BU_BAT and MyParam CONTAINS BAT', my_dict,
                                       QUALYS_COMPARATORS_DICT['am']) is False

        assert utils.query_verify_dict('not a valid query', my_dict) is False
        assert utils.query_verify_dict('cache view', my_dict) is True
        assert utils.query_verify_dict('cache view another_param', my_dict) is False
        assert utils.query_verify_dict('bookmarks run 89', my_dict) is True

    def test_extract_dict_level(self):
        import utils
        my_dict = {'a': {'aa': 'aa', 'ab': {'aba': 'aba'}}, 'b': 'b',
                   'c': {'ca': 'ca', 'cb': {'cba': 'cba', 'cbb': 'cbb'}, 'cd': 'cd'}}

        res = utils.extract_dict_level(my_dict, 0)
        assert res == ['a', 'b', 'c']

        res = utils.extract_dict_level(my_dict, 1)
        assert res == ['aa', 'ab', 'ca', 'cb', 'cd']

        res = utils.extract_dict_level(my_dict, 2)
        assert res == ['aba', 'cba', 'cbb']

    def test_extract_real_dict_level(self):
        import utils, completion

        tools = list(set(utils.extract_dict_level(completion.QUERY_DICT_QUALYS, 0)))
        assert 'am' in tools

        verbs = list(set(utils.extract_dict_level(completion.QUERY_DICT_QUALYS, 1)))
        assert 'search' in verbs

        objects = list(set(utils.extract_dict_level(completion.QUERY_DICT_QUALYS, 2)))
        assert 'host/vm/detection' in objects

if __name__ == '__main__':
    unittest.main()